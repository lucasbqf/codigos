//SPACERUN 3.2
// SPACE RUN 3.0 THIS SHALL RUN MOTHA FOCKA!!!
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <windows.h>
#include <semaphore.h>
#include <time.h> 
#pragma comment(lib, "Winmm.lib")
#include <mmsystem.h>

// funcao de colocar o handler em certa posicao
void gotoxy(int x, int y){
     SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),(COORD){x,y});
}

//defines de coordenadas
#define maxWallX 50
#define maxWallY 50
#define startXPlayer (maxWallX/2)
#define startYPlayer (maxWallY-5)


//desenhos
#define wall 219
#define center 202
#define asad 207
#define asae 207
#define clear 32

//botoes
#define up 72
#define down 80
#define left 75
#define right 77


//globais de controle
int keypressed;
char lado;
double frames;
sem_t vez;
sem_t descida;
int buffer; 
int velocidade=0;


  ///////////////////////////////////////
 ///definicoes dos "objetos" do  jogo///
///////////////////////////////////////
//variaveis do jogador
typedef struct player{
	// centro player
	int xc,yc;
	//asa direta
	int xd,yd;
	//asa esquerda
	int xe,ye;
	//informacoes de vida
	int vivo;
	int hp;
	//arma
	int heat;
}TpPlayer;


  //////////////////////
 //inicio das funcoes//
//////////////////////

//inicializando um jogador
TpPlayer jogador;
//informacoes do jogo
void infos(){
	gotoxy(50,51);
		printf("x=%d,y=%d,vida=%d",jogador.xc,jogador.yc,jogador.hp);
	gotoxy(50,52);
		printf("%c",lado);
	gotoxy(50,53);
		printf("%d",velocidade);
}

//posicao inicial  do jogador
void startplayer(){
	jogador.xc=startXPlayer;
	jogador.yc=startYPlayer;
	jogador.xd=jogador.xc+1;
	jogador.yd=jogador.yc;
	jogador.xe=jogador.xc-1;
	jogador.ye=jogador.yc;
	jogador.hp=50;
}

// movimentar o player para os lados
void moveplayer(){
	if(keypressed==right && jogador.xd<maxWallX-1)
		jogador.xc++;
	
	if(keypressed==left &&jogador.xe>1)
		jogador.xc--;
		
	jogador.xd=jogador.xc+1;
	jogador.xe=jogador.xc-1;
}

//quando andar, retorna os valores dos lado
char erase(){
	if(keypressed==right){
		return 'l';
	}
	if(keypressed==left){
		return 'r';
	}
}
	
	
  /////////////////////
 //FUNCOES DE PRINTS//
/////////////////////

// apagar nas posicoes x o jogador
void erasePlayer(char lado){
	if(lado=='l'){
		gotoxy(jogador.xe-1,jogador.ye);
		putchar(32);
	}
	if(lado=='r'){
		gotoxy(jogador.xd+1,jogador.yd);
		putchar(32);
	}
}
//funcao de desenho do jogador
void printplayer(){
	gotoxy(jogador.xc,jogador.yc);
	putchar(center);
	gotoxy(jogador.xd,jogador.yd);
	putchar(asad);
	gotoxy(jogador.xe,jogador.ye);
	putchar(asae);
	
}

// barreiras para o mapa;
void barreiras(){
	int x, y;
	for(x=0;x<maxWallX;x++){
		gotoxy(x,0);
		putchar(wall);
		gotoxy(x,maxWallY);
		putchar(wall);
	}
	for(y=0;y<=maxWallY;y++){
		gotoxy(0,y);
		putchar(wall);
		gotoxy(maxWallX,y);
		putchar(wall);
	}
}
//thread para pegar a tecla pressionada
void *keyPress(void *arg){
	while(true){
			sem_wait(&vez); 
			if(kbhit()==1)
				keypressed=getch();
			lado=erase();
			fflush(stdin);
			moveplayer();
			
			sem_post(&vez);
			Sleep(3);
			keypressed=0;
	}
}
// thread de impressao na tela
void *Printer(void *arg){
	while(true){
			sem_wait(&vez); 
			printplayer();
			erasePlayer(lado);
			infos();
			lado='0';
			Sleep(3);
			sem_post(&vez);
	}
}

int main(){
	sem_init(&vez, 0, 1);
	pthread_t key;
	pthread_t pl;
	barreiras();	
	startplayer();
	
	//thread de impressao de tudo q tem q ser impresso em tela
	pthread_create(&pl,NULL,Printer,NULL);
	//thread de para execucao de leitura de informacoes do teclado
	pthread_create(&key,NULL,keyPress,NULL);
	
	while(TRUE){
		Sleep(1000);
		if(velocidade < 180)
			velocidade++;
	}	
}
