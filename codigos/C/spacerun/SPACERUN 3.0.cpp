// SPACE RUN 3.0 THIS SHALL RUN MOTHA FOCKA!!!
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <windows.h>
#include <semaphore.h>
#include <time.h>  

// funcao de colocar o handler em certa posicao
void gotoxy(int x, int y){
     SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),(COORD){x,y});
}

/*void gotoxy (int x, int y){
    COORD coord; // coordinates
    coord.X = x; coord.Y = y; // X and Y coordinates
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}*/


//globais de controle
int keypressed;
char lado;
double frames;
sem_t vez;
sem_t descida;
int buffer; 





//defines de coordenadas
#define maxWallX 50
#define maxWallY 50
#define startXPlayer (maxWallX/2)
#define startYPlayer (maxWallY-3)


//desenhos
#define wall 219
#define center 202
#define asad 207
#define asae 207
#define clear 32

//botoes
#define up 72
#define down 80
#define left 75
#define right 77

//velocidade maxima do asteroide
#define maxVel 50


  ///////////////////////////////////////
 ///definicoes dos "objetos" do  jogo///
///////////////////////////////////////
//variaveis do jogador
typedef struct player{
	// centro player
	int xc,yc;
	//asa direta
	int xd,yd;
	//asa esquerda
	int xe,ye;
	//informacoes de vida
	int vivo;
	int hp;
	//arma
	int cooling;
}TpPlayer;

typedef struct tiro{
	//coordenada do tiro
	int x,y;
	// tiro ainda vivo
	int vivo;
}TpTiro;

typedef struct asteroide{
	//coordenadas do asteroide
	int x,y;
	//esta vivo?
	int vivo;
	int velocidade;
	
	asteroide *proximo;
	
}TpAsteroide;


  //////////////////////
 //inicio das funcoes//
//////////////////////

//inicializando um jogador
TpPlayer jogador;
//informacoes do jogo
void infos(){
	gotoxy(50,51);
		printf("%d",keypressed);
	gotoxy(50,52);
		printf("%c",lado);
}

//posicao inicial  do jogador
void startplayer(){
	jogador.xc=startXPlayer;
	jogador.yc=startYPlayer;
	jogador.xd=jogador.xc+1;
	jogador.yd=jogador.yc;
	jogador.xe=jogador.xc-1;
	jogador.ye=jogador.yc;
}


//ponterios dos meteoros
TpAsteroide *cabecaAst;




// movimentar o player para os lados
void moveplayer(){
	if(keypressed==right && jogador.xd<maxWallX-1)
		jogador.xc++;
	
	if(keypressed==left &&jogador.xe>1)
		jogador.xc--;
		
	jogador.xd=jogador.xc+1;
	jogador.xe=jogador.xc-1;
}

//quando andar, retorna os valores dos lado
char erase(){
	if(keypressed==right){
		return 'l';
	}
	if(keypressed==left){
		return 'r';
	}
}
// apagar nas posicoes x o jogador
void erasePlayer(char lado){
	if(lado=='l'){
		gotoxy(jogador.xe-1,jogador.ye);
		putchar(32);
	}
	if(lado=='r'){
		gotoxy(jogador.xd+1,jogador.yd);
		putchar(32);
	}
}
//funcao de desenho do jogador
void printplayer(){
	gotoxy(jogador.xc,jogador.yc);
	putchar(center);
	gotoxy(jogador.xd,jogador.yd);
	putchar(asad);
	gotoxy(jogador.xe,jogador.ye);
	putchar(asae);
	
}
// barreiras para o mapa;
void barreiras(){
	int x, y;
	for(x=0;x<maxWallX;x++){
		gotoxy(x,0);
		putchar(wall);
		gotoxy(x,maxWallY);
		putchar(wall);
	}
	for(y=0;y<=maxWallY;y++){
		gotoxy(0,y);
		putchar(wall);
		gotoxy(maxWallX,y);
		putchar(wall);
	}
}


  /////////////////////////////
 //FUNCS ALOCACAO DE MEMORIA//
/////////////////////////////

//alocacao de memoria
void allocAst(TpAsteroide **nvasteroide){
    (*nvasteroide) = (TpAsteroide *) malloc(sizeof(TpAsteroide));
    (*nvasteroide)->proximo = NULL;
  //  printf("ALOCACAO FEITA");//
}

//inicializacao do asteroide
void randAsteroid(TpAsteroide *asteroide){
	asteroide->x = (rand() % 1+50);
	asteroide->y = 1;
	asteroide->velocidade=1;
	asteroide->vivo=1;
//	printf("\n INICIALIZACAO FEITA");//
}
//colocada do novo asteroide na lista
void colocaAstLista(TpAsteroide *cabeca,TpAsteroide *nvasteroide){
	TpAsteroide *cursor;
	
	cursor = cabeca;
	
	while(cursor->proximo!= NULL){
		cursor = cursor->proximo;
	}
	cursor->proximo =nvasteroide;
	//printf("\n\nCOLOCACAO FEITA FEITA");//
}

//descer uma posicao nos asteroides da lista
void descerAsteroide(TpAsteroide *cabeca){
	TpAsteroide *cursor;
	
	cursor = cabeca;
	while(cursor->proximo!= NULL){
		cursor->y = cursor->y+1;
		cursor = cursor->proximo;
	}
		if(cursor->y==49){
			cabeca->proximo=cursor->proximo;
			free(cursor);
		//	printf("\n\n\n descida FEITA");//
	}
}
//printar asteroide
void printAst(TpAsteroide *cabeca){
	TpAsteroide *cursor;
	
	cursor = cabeca;
	while(cursor->proximo!= NULL){
		gotoxy(cursor->x,cursor->y);
		putch(92);
		cursor = cursor->proximo;
	}
	Sleep(3);
}


  ////////////
 //THREADS!//
////////////



//thread para pegar a tecla pressionada
void *keyPress(void *arg){
	while(true){
			sem_wait(&vez); 
			if(kbhit()==1)
				keypressed=getch();
			lado=erase();
			fflush(stdin);
			moveplayer();
			sem_post(&vez);
			Sleep(3);
			keypressed=0;
	}
}

//thread para os asteroides
void *Asteroides(void *arg){
	TpAsteroide *novoAsteroide;
	while(true){
		//printf("hail!");//
		sem_wait(&vez);
		//printf("passou o semafaro");//
		allocAst(&novoAsteroide);
		randAsteroid(novoAsteroide);
		colocaAstLista(cabecaAst,novoAsteroide);
		descerAsteroide(cabecaAst);
		Sleep(3);
		sem_post(&vez);
	}
	
}

// thread de impressao na tela
void *Printer(void *arg){
	while(true){
			sem_wait(&vez); 
			printplayer();
			erasePlayer(lado);
			printAst(cabecaAst);
			infos();
			lado='0';
			Sleep(3);
			vez=0;
			sem_post(&vez);
	}
}




int main(){
	allocAst(&cabecaAst);
	
	
	//inicializacao do semafaro
	sem_init(&vez, 0, 1);
	sem_init(&descida,0,1);
	
	//rate do teclado para deixa-lo mais  rapido
	system("mode con: rate=32 delay=0");
	
	
	// inicializacao da lista encadeada de meteoros 

	//inicializacao do random para os asteroides
	srand (time(NULL));
	
///////////////////////////////////////////////////
	//ponteiros para os threads
	pthread_t key;
	pthread_t pl;
	pthread_t asteroides;
///////////////////////////////////////////////////
	//inicializacao de prints entre outros
	barreiras();	
	startplayer();
/////////////////////////////////////////////////
//execucao das threads
	//thread de para execucao de leitura de informacoes do teclado
	pthread_create(&key,NULL,keyPress,NULL);
	
	//thread de impressao de tudo q tem q ser impresso em tela
	pthread_create(&pl,NULL,Printer,NULL);
	//thread de execucao de criacao e movimentacao dos asteroides
//	printf("thread do  asteroide");//
	pthread_create(&asteroides,NULL,Asteroides,NULL);
//	printf("thread iniciada");//
///////////////////////////////////////////////

	while(TRUE){
		Sleep(30);
	}	
}
