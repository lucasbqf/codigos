// SPACE RUN 3.0 THIS SHALL RUN MOTHA FOCKA!!!
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <windows.h>
#include <semaphore.h>
#include <time.h> 
#pragma comment(lib, "Winmm.lib")
#include <mmsystem.h>

// funcao de colocar o handler em certa posicao
void gotoxy(int x, int y){
     SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),(COORD){x,y});
}


//globais de controle
int keypressed;
char lado;
double frames;
sem_t vez;
sem_t descida;
int buffer; 
int velocidade=0;





//defines de coordenadas
#define maxWallX 50
#define maxWallY 50
#define startXPlayer (maxWallX/2)
#define startYPlayer (maxWallY-5)


//desenhos
#define wall 219
#define center 202
#define asad 207
#define asae 207
#define clear 32

//botoes
#define up 72
#define down 80
#define left 75
#define right 77

//velocidade maxima do asteroide
#define maxVel 50


  ///////////////////////////////////////
 ///definicoes dos "objetos" do  jogo///
///////////////////////////////////////
//variaveis do jogador
typedef struct player{
	// centro player
	int xc,yc;
	//asa direta
	int xd,yd;
	//asa esquerda
	int xe,ye;
	//informacoes de vida
	int vivo;
	int hp;
	//arma
	int heat;
}TpPlayer;

typedef struct tiro{
	//coordenada do tiro
	int x,y;
	// tiro ainda vivo
	int vivo;
}TpTiro;

typedef struct asteroide{
	//coordenadas do asteroide
	int x,y;
	//esta vivo?
	int vivo;
	int vida;
	asteroide *proximo;
	
}TpAsteroide;


  //////////////////////
 //inicio das funcoes//
//////////////////////

//inicializando um jogador
TpPlayer jogador;
//informacoes do jogo
void infos(){
	gotoxy(50,51);
		printf("x=%d,y=%d,vida=%d",jogador.xc,jogador.yc,jogador.hp);
	gotoxy(50,52);
		printf("%c",lado);
	gotoxy(50,53);
		printf("%d",velocidade);
}

//posicao inicial  do jogador
void startplayer(){
	jogador.xc=startXPlayer;
	jogador.yc=startYPlayer;
	jogador.xd=jogador.xc+1;
	jogador.yd=jogador.yc;
	jogador.xe=jogador.xc-1;
	jogador.ye=jogador.yc;
	jogador.hp=50;
}


//ponterios dos meteoros
TpAsteroide *cabecaAst;




// movimentar o player para os lados
void moveplayer(){
	if(keypressed==right && jogador.xd<maxWallX-1)
		jogador.xc++;
	
	if(keypressed==left &&jogador.xe>1)
		jogador.xc--;
		
	jogador.xd=jogador.xc+1;
	jogador.xe=jogador.xc-1;
}

//quando andar, retorna os valores dos lado
char erase(){
	if(keypressed==right){
		return 'l';
	}
	if(keypressed==left){
		return 'r';
	}
}

// funcao quando um asteroide bate no jogador
void hitPlayer(TpAsteroide *cursor){
	if((cursor->x==jogador.xc ||cursor->x+1==jogador.xc||cursor->x-1==jogador.xc)&& (cursor->y==jogador.ye))
		jogador.hp=jogador.hp-5;
}



  /////////////////////
 //FUNCOES DE PRINTS//
/////////////////////

// apagar nas posicoes x o jogador
void erasePlayer(char lado){
	if(lado=='l'){
		gotoxy(jogador.xe-1,jogador.ye);
		putchar(32);
	}
	if(lado=='r'){
		gotoxy(jogador.xd+1,jogador.yd);
		putchar(32);
	}
}
//funcao de desenho do jogador
void printplayer(){
	gotoxy(jogador.xc,jogador.yc);
	putchar(center);
	gotoxy(jogador.xd,jogador.yd);
	putchar(asad);
	gotoxy(jogador.xe,jogador.ye);
	putchar(asae);
	
}
// barreiras para o mapa;
void barreiras(){
	int x, y;
	for(x=0;x<maxWallX;x++){
		gotoxy(x,0);
		putchar(wall);
		gotoxy(x,maxWallY);
		putchar(wall);
	}
	for(y=0;y<=maxWallY;y++){
		gotoxy(0,y);
		putchar(wall);
		gotoxy(maxWallX,y);
		putchar(wall);
	}
}



  /////////////////////////////
 //FUNCS ALOCACAO DE MEMORIA//
/////////////////////////////

//alocacao de memoria
void allocAst(TpAsteroide **nvasteroide){
    (*nvasteroide) = (TpAsteroide *) malloc(sizeof(TpAsteroide));
    (*nvasteroide)->proximo = NULL;
  //  printf("ALOCACAO FEITA");//
}

//inicializacao do asteroide
void randAsteroid(TpAsteroide *asteroide){
	asteroide->x = 2+(rand() %47);
	asteroide->y = 2;
	asteroide->vivo=1;
	asteroide->vida=3;
}
//colocada do novo asteroide na lista
void colocaAstLista(TpAsteroide *cabeca,TpAsteroide *nvasteroide){
	TpAsteroide *cursor;
	
	cursor = cabeca;
	
	while(cursor->proximo!= NULL){
		cursor = cursor->proximo;
	}
	cursor->proximo =nvasteroide;
}

//descer uma posicao nos asteroides da lista
void descerAsteroide(TpAsteroide *cabeca){
	TpAsteroide *cursor;
	TpAsteroide *apagaCursor;
	
	cursor = cabeca->proximo;
	while(cursor->proximo!= NULL){
		if(cursor->y==48){
			apagaCursor=cursor;
			cabeca->proximo=cursor->proximo;
			cursor = cursor->proximo;
			free(apagaCursor);
		}
		cursor->y = cursor->y+1;
		cursor = cursor->proximo;
		hitPlayer(cursor);
	}

}
//limpar as posicoes dos asteroides


void limpaAst(TpAsteroide *cabeca){
	TpAsteroide *cursor;
	
	cursor = cabeca->proximo;
	while(cursor->proximo!= NULL){
		gotoxy(cursor->x,cursor->y-2);
		putch(32);
		gotoxy(cursor->x-1,cursor->y-1);
		putch(32);
		gotoxy(cursor->x+1,cursor->y-1);
		putch(32);
		cursor = cursor->proximo;
	}
}

void deletaLista(TpAsteroide *cabeca){
	TpAsteroide *cursor;
	TpAsteroide *cursorDeleta;
	cursor=cabeca;
	while(cursor->proximo!=NULL){
		cursorDeleta=cursor;
		cursor=cursor->proximo;		
		free(cursorDeleta);
	}
}


//printar asteroide
void printAst(TpAsteroide *cabeca){
	TpAsteroide *cursor;
	
	cursor = cabeca->proximo;
	while(cursor->proximo!= NULL){
		gotoxy(cursor->x-1,cursor->y);
		printf("%c%c%c",178,178,178);
		gotoxy(cursor->x,cursor->y-1);
		printf("%c",178);
		gotoxy(cursor->x,cursor->y+1);
		printf("%c",178);
		cursor = cursor->proximo;
	}
	printf("\n\n");
	Sleep(3);
	
}
void limpaFim(){
	gotoxy(1,47);
	printf("                                                 ");
	gotoxy(1,48);
	printf("                                                 ");
	gotoxy(1,49);
	printf("                                                 ");
}







  ////////////
 //THREADS!//
////////////



//thread para pegar a tecla pressionada
void *keyPress(void *arg){
	while(true){
			sem_wait(&vez); 
			if(kbhit()==1)
				keypressed=getch();
			lado=erase();
			fflush(stdin);
			moveplayer();
			
			sem_post(&vez);
			Sleep(3);
			keypressed=0;
	}
}

//thread para os asteroides
void *Asteroides(void *arg){
	TpAsteroide *novoAsteroide;
	while(true){
		sem_wait(&vez);
		allocAst(&novoAsteroide);
		randAsteroid(novoAsteroide);
		colocaAstLista(cabecaAst,novoAsteroide);
		descerAsteroide(cabecaAst);
		sem_post(&vez);
		Sleep(200-velocidade);
	}
	
}

// thread de impressao na tela
void *Printer(void *arg){
	while(true){
			sem_wait(&vez); 
			printplayer();
			erasePlayer(lado);
			infos();
			limpaAst(cabecaAst);
			printAst(cabecaAst);
			limpaFim();
			lado='0';
			Sleep(3);
			sem_post(&vez);
	}
}


void *musicas(void *arg){
	//PlaySound(TEXT("DCHi.wav"), NULL, SND_SYNC);
	Sleep(100);
}

int main(){
	//alocacao  da cabe�a do asteroide
	allocAst(&cabecaAst);
	//inicializacao do semafaro
	sem_init(&vez, 0, 1);
	sem_init(&descida,0,1);
	//rate do teclado para deixa-lo mais  rapido
	system("mode con: rate=32 delay=0");
	//inicializacao do random para os asteroides
	srand (time(NULL));
	
// inicializacao da lista encadeada de meteoros 
	///////////////////////////////////////////////////
	//ponteiros para os threads
	pthread_t key;
	pthread_t pl;
	pthread_t asteroides;
	pthread_t musica;
	///////////////////////////////////////////////////
//inicializacao de prints entre outros
	barreiras();	
	startplayer();
//execucao das threads

	//thread de execucao de criacao e movimentacao dos asteroides
	pthread_create(&asteroides,NULL,Asteroides,NULL);
	//thread de impressao de tudo q tem q ser impresso em tela
	pthread_create(&pl,NULL,Printer,NULL);
	//thread de para execucao de leitura de informacoes do teclado
	pthread_create(&key,NULL,keyPress,NULL);
	
	//thead de musica
	pthread_create(&musica,NULL,musicas,NULL);
	
	while(TRUE){
		Sleep(1000);
		if(velocidade < 180)
			velocidade++;
	}	
}
