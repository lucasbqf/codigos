package loneranger;

import robocode.*;
import java.awt.Color;
//import utils;
import static robocode.util.Utils.*;//normalRelativeAngleDegrees;
// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * Hadou - a robot by (lucasbqf)
 */
public class Hadou extends Robot {
	double vida;
	double gunTurnAmt, enemyEnergy, enemyEnergyOld;
	double rand = 0, intRand;
	int direcao = 1;
	double tirodado = 0, tiroerrado = 0, mediatiro = 0;
	boolean desvia = false;
	int scanDirection = 1;

	public void dodge() {
		desvia = false;
		ahead(50 * direcao);
	}

	public void limitante() {
		if (getX() < 250 || getX() > 550 || getY() < 250 || getY() > 550) {
			if (direcao == 1)
				direcao = -1;
			else
				direcao = 1;
		}
	}

	public void run() {
		// setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
		setColors(Color.red, Color.black, Color.black); // body,gun,radar
		turnGunLeft(90);
		while (true) {
			turnRadarRight(gunTurnAmt);
			limitante();
			turnRadarRight(360);
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		if (e.isSentryRobot() == false) {
			enemyEnergy = e.getEnergy();
			gunTurnAmt = normalRelativeAngleDegrees(e.getBearing() + (getHeading() - getRadarHeading()));
			turnRadarRight(gunTurnAmt);
			turnRight(normalRelativeAngleDegrees(e.getBearing() + 90));
			if (getGunHeat() == 0) {
				// if(e.getVelocity()==0){
				fireBullet(600 / e.getDistance());
				tirodado = tirodado + 1;
				mediatiro = (tiroerrado / tirodado) * 100;
				System.out.println("porcentagem " + mediatiro);
				// }
			}
			if (e.getEnergy() >= enemyEnergyOld - 3 && enemyEnergy < enemyEnergyOld) {
				dodge();
			}
			enemyEnergyOld = enemyEnergy;
			turnRadarRight(30 * scanDirection);
			turnRadarRight(30 * -scanDirection);
			scanDirection *= -1;
		}
	}

	public void onHitByBullet(HitByBulletEvent e) {
		if (vida > getEnergy() - 3) {
			turnRadarRight(e.getBearing() - getRadarHeading() + getHeading());
		}
	}

	public void onHitRobot(HitRobotEvent e) {
		// turnGunRight(normalRelativeAngleDegrees(e.getBearing()));
	}

	public void onBulletMissed(BulletMissedEvent e) {
		tiroerrado = tiroerrado + 1;
	}
}
