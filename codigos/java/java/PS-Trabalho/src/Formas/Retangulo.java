package Formas;

public class Retangulo extends Formageometrica{
	private Ponto p;
	private float base, altura;
	
	public Retangulo(){
		this.p = new Ponto(0,0);
		this.base = 2.0f;
		this.altura = 1.0f;
	}
	
	public Retangulo(Ponto p, float b, float a)throws Exception{
		if (p == null){
			Exception ex = new Exception ("Valor invalido");	
			throw ex;
		}
		//construtor especifico
		this.p=p;
		base=b;
		altura=a;
	}
	
	public Retangulo(Retangulo r){
		p = new Ponto(r.p);
		base = r.base;
		altura = r.altura;
	}

	public Ponto getP() {
		return p;
	}

	public void setP(Ponto p) {
		this.p = p;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	public float area(){
		return base*altura;
	}
	
	public float perimetro(){
		return 2*(base+altura);
	}
	
	public Ponto centro(){
		float x,y;
		x = (p.getX()+base)/2;
		y = (p.getY()+base)/2;
		return new Ponto(x,y);
	}
	
	public String toString(){
	return "Retangulo;" +p+";"+base+";"+altura;
	}
}
