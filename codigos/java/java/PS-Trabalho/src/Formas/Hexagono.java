package Formas;
import java.util.*;
public class Hexagono {
	private Ponto p1;
	private Ponto p2;
	private float X1;
	private float Y2;
	private float lado;
	
	
	public Hexagono(Ponto p1,Ponto p2){
		this.p1 = p1;
		this.p2 = p2;
		this.X1 = p1.getX()*p1.getX();
		this.Y2 = p2.getX()*p2.getX();
		this.lado = (float) Math.sqrt(X1+Y2);		
	}
	public float area(){
		return (float) ((3*lado*lado*Math.sqrt(3))/2);
				
	}
	public float perimetro(){
		return lado*6;
	}
	
	public String toString(){
	return "Hexagono;"+"ponto"+p1+"p2"+p2+"lado"+lado;
	}
	
	
	
	
}
