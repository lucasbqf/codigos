package Formas;

public class Triangulo extends Formageometrica{
	private Ponto p;
	private Ponto p2;
	private Ponto p3;
	private float base, altura;
	
	public Triangulo(){
		p = new Ponto(0,0);
		this.base = 2.0f;
		this.altura = 1.0f;
	}
	
	public Triangulo(Ponto p, float a, float b){
		//construtor especifico
		this.p=p;
		base=b;
		altura=a;
	}
	
	public Triangulo(Ponto p1, Ponto p2, Ponto p3){
		this.p = p1;
		this.p2= p2;
		this.p3= p3;
	}
	
	public Triangulo(Triangulo t){
		p = new Ponto(t.p);
		base = t.base;
		altura = t.altura;
	}

	public Ponto getP() {
		return p;
	}

	public void setP(Ponto p) {
		this.p = p;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	public float area(){
		return (base*altura)/2;
	}
	
	public float perimetro(){
		float x;
		x = (altura*altura)+(base*base);
		return base+altura+(float)Math.sqrt(x);
	}
	
	public Ponto centro(){
		float x,y;
		x = (p.getX()+base)/2;
		y = (p.getY()+base)/2;
		return new Ponto(x,y);
	}
	
	@Override
	public String toString() {
	return "Triangulo;" + p  +";"+ p2 + ";"+ p3 +"";
	}
}
