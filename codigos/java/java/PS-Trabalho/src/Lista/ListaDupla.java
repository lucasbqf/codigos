package Lista;

public class ListaDupla {

	private No primeira;

	private No ultima;
	private int totalDeElementos = 0;

	public void adiciona(Object elemento) {
		if (this.totalDeElementos == 0) {
			No nova = new No(this.primeira, elemento);
			this.primeira = nova;
			this.ultima = this.primeira;
			this.totalDeElementos++;

		} else {
			No nova = new No(elemento);
			this.ultima.setProxima(nova);
			this.ultima = nova;
			this.totalDeElementos++;
		}
	}

	private No retornaNo(int posicao) {
		No atual = primeira;
		for (int i = 0; i < posicao; i++) {
			atual = atual.getProxima();
		}
		return atual;
	}

	public void remove(int posicao) {

		No anterior = this.retornaNo(posicao);
		No atual = anterior.getProxima();
		No proxima = atual.getProxima();

		anterior.setProxima(proxima);
		proxima.setAnterior(anterior);

		this.totalDeElementos--;

	}
	public void listar(){
		IteradorListaDupla it = new IteradorListaDupla(primeira);
		
		while(it.hasANext()){			
			System.out.println(it.dado());
			it.proximo();
		}
		System.out.println(it.dado());
	}


	public class IteradorListaDupla extends Iterador {

		private No indice;

		public IteradorListaDupla(No indice) {
			this.indice = indice;
		}

		@Override
		public void proximo() {
			indice = indice.getProxima();
			// return indice;
		}

		@Override
		public Object anterior() {
			indice = indice.getAnterior();
			return indice.getAnterior();
		}

		@Override
		public Object dado() {
			// TODO Auto-generated method stub
			return indice.getElemento();
		}

		@Override
		public boolean hasANext() {

			if (indice.getProxima() == null) {
				return false;
			} else {
				return true;
			}

		}

		@Override
		public boolean hasAprev() {
			// TODO Auto-generated method stub
			if (indice.getAnterior() == null) {
				return false;
			} else {
				return true;
			}
		}

	}

}