package Lista;

public abstract class Iterador {
	
	public abstract void proximo();
	public abstract Object anterior();
	public abstract Object dado();
	public abstract boolean hasANext();
	public abstract boolean hasAprev();


	
	

}
