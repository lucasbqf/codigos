package Lista;

public class No {
	private No proxima;
	private No anterior;
	  private Object elemento;

	  public No(No proxima, Object elemento) {
	    this.proxima = proxima;
	    this.elemento = elemento;
	  }

	  public No(Object elemento) {
	    this.elemento = elemento;
	  }

	  public void setProxima(No proxima) {
	    this.proxima = proxima;
	  }
	  public void setAnterior(No anterior) {
		    this.anterior = anterior;
	  }
	  public No getAnterior(){
		  return this.anterior;
	  }
	  
	  public No getProxima() {
	    return proxima;
	  }
	  
	  public Object getElemento() {
	    return elemento;
	  }
}
