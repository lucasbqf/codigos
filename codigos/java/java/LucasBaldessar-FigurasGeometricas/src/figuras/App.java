package figuras;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class App {
	private ListaDupla listaFiguras;
	private Scanner sc;

	public static void main(String[] args) {
		new App();
	}

	public App() {
		listaFiguras = new ListaDupla();
		sc = new Scanner(System.in);

		menuPrincipal();

		sc.close();
	}

	private void menuPrincipal() {
		int op = 1;
		while (op != 0) {
			System.out.println("0 - Sair");
			System.out.println("1 - incluir");
			System.out.println("2 - excluir");
			System.out.println("3 - listar");
			System.out.println("4 - distancias at� um ponto");
			System.out.println("5 - distancias at� uma figura");
			System.out.println("7 - perimetros das figuras");
			System.out.println("8 - listar com iterador");
			System.out.println("9 - criar arquivo de texto");
			System.out.println(":");
			op = sc.nextInt();

			switch (op) {
			case 0:
				return;
			case 1:
				menuInserirFiguras();
				break;
			case 2:
				excluirFigura();
				break;
			case 3:
				listarFiguras();
				break;
			case 4:
				distanciasPonto();
				break;
			case 5:
				distanciasFigura();
				break;
			case 7:
				perimetroFiguras();
				break;
			case 8:
				listarFigurasIterador();
				break;
			case 9:
				criarArquivoTexto();
				break;
			}
		}
	}

	private void criarArquivoTexto() {
		FileWriter output;

		try {
			output = new FileWriter("h:/figuras.txt");
		} catch (IOException e) {
			// e.printStackTrace();
			System.out.println(e.getMessage());
			return;
		}

		Iterador i = listaFiguras.getCabeca();
		FiguraGeometrica f;
		while ((f = (FiguraGeometrica) i.proximo()) != null) {
			try {
				output.append(String.format("%s\n", f.toString()));
			} catch (IOException e) {
				// e.printStackTrace();
				System.out.println(e.getMessage());
				break;
			}
		}

		try {
			output.close();
		} catch (IOException e) {
			// e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	private void menuInserirFiguras() {
		System.out.println("1 - quadrado");
		System.out.println("2 - triangulo");
		System.out.println("3 - poligono");
		System.out.println(":");
		int op = sc.nextInt();
		switch (op) {
		case 1:
			insereQuadrado();
			break;
		case 2:
			insereTriangulo();
			break;
		case 3:
			inserePoligono();
			break;
		}
	}

	private void insereQuadrado() {
		Ponto a = lerPonto("coordenadas para primeiro ponto:");
		Ponto b = lerPonto("coordenadas para segundo ponto:");
		Ponto c = lerPonto("coordenadas para terceiro ponto:");
		Ponto d = lerPonto("coordenadas para quarto ponto:");

		try {
			Quadrado q = new Quadrado(a, b, c, d);
			listaFiguras.insere(q, 0);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Triangulo n�o inserido na lista de figuras.");
		}
	}

	private void insereTriangulo() {
		Ponto a = lerPonto("coordenadas para primeiro ponto:");
		Ponto b = lerPonto("coordenadas para segundo ponto:");
		Ponto c = lerPonto("coordenadas para terceiro ponto:");

		try {
			Triangulo t = new Triangulo(a, b, c);
			listaFiguras.insere(t, 0);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Triangulo n�o inserido na lista de figuras.");
		}
	}

	private void inserePoligono() {
		System.out.println("poligono de quantos pontos:");
		int q = sc.nextInt();
		ListaDupla lista = new ListaDupla();
		for (int i = 0; i < q; i++) {
			Ponto a = lerPonto("coordenadas para o ponto " + (i + 1) + ":");
			lista.insereFim(a);
		}
		try {
			Poligono p = new Poligono(lista);
			listaFiguras.insere(p, 0);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Poligono n�o inserido na lista de figuras.");
		}
	}

	private Ponto lerPonto(String msg) {
		System.out.println(msg);
		float x = sc.nextFloat();
		float y = sc.nextFloat();
		return new Ponto(x, y);
	}

	private void excluirFigura() {
		System.out.println("qual o indice a ser excluido:");
		int i = sc.nextInt();
		FiguraGeometrica f = (FiguraGeometrica) listaFiguras.remove(i);
		System.out.println("Objeto removido: " + f);
	}

	private void listarFiguras() {
		System.out.println("Figuras cadastradas:");
		for (int i = 0; i < listaFiguras.getTamanho(); i++)
			System.out.println(listaFiguras.consulta(i));
	}

	private void listarFigurasIterador() {
		System.out.println("Figuras cadastradas:");

		Iterador i = listaFiguras.getInicio();
		FiguraGeometrica f;
		while ((f = (FiguraGeometrica) i.proximo()) != null)
			System.out.println(f);
	}

	private void distanciasPonto() {
		Ponto a = lerPonto("coordenadas do ponto para calcular as distancias:");
		System.out.println("distancias do ponto at� as figuras cadastradas:");
		for (int i = 0; i < listaFiguras.getTamanho(); i++)
			System.out.printf("Figura %d -> Distancia %f \n", i,
					((FiguraGeometrica) listaFiguras.consulta(i)).distancia(a));
	}

	private void distanciasFigura() {
		System.out.println("qual o indice da figura a ser utilizada no calculo de distancias:");
		int i = sc.nextInt();
		FiguraGeometrica f = (FiguraGeometrica) listaFiguras.consulta(i);
		System.out.printf("distancias da figura %s at� as figuras cadastradas:\n", f);
		for (i = 0; i < listaFiguras.getTamanho(); i++)
			System.out.printf("Figura %d -> Distancia %f \n", i,
					((FiguraGeometrica) listaFiguras.consulta(i)).distancia(f.centro()));
	}

	private void perimetroFiguras() {
		System.out.println("perimetros das figuras cadastradas:");
		for (int i = 0; i < listaFiguras.getTamanho(); i++)
			System.out.printf("Figura %d -> Perimetro %f \n", i,
					((FiguraGeometrica) listaFiguras.consulta(i)).perimetro());
	}
}