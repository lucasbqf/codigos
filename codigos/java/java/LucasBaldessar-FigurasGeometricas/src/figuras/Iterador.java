package figuras;

public abstract class Iterador {

	public abstract Object proximo();

	public abstract Object anterior();

	public abstract Object dado();

	public abstract boolean temProximo();

	public abstract boolean temAnterior();

}
