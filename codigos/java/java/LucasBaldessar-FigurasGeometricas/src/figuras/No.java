package figuras;

public class No {
	private No proximo;
	private No anterior;
	private Object dados;

	/*public No(No proximo, Object dados) {
		this.proximo = proximo;
		this.dados = dados;
	}*/

	public No(Object dados) {
		this.dados = dados;
	}

	public void setProximo(No proximo) {
		this.proximo = proximo;
	}

	public void setAnterior(No anterior) {
		this.anterior = anterior;
	}

	public No getAnterior() {
		return this.anterior;
	}

	public No getProximo() {
		return this.proximo;
	}

	public Object getDados() {
		return this.dados;
	}

}
