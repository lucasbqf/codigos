package figuras;
public abstract class FiguraGeometrica {
	abstract public float area();

	abstract public float perimetro();

	abstract public Ponto centro();

	public float distancia(Ponto ponto) {
		return centro().distancia(ponto);
	}

	public float distancia(FiguraGeometrica figura) {
		return centro().distancia(figura.centro());
	}

	abstract public String toString();
}