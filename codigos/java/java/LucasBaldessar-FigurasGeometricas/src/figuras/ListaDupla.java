package figuras;

public class ListaDupla {
	private No cabeca;
	private No fim;
	private int tamanhoLista = 0;
	
	public ListaDupla(){
		cabeca=null;
		fim=null;
	}
	
	public void adicionar(Object dados) {
		if (tamanhoLista == 0) {
			No novoNo = new No(dados);
			cabeca = fim = novoNo;
			tamanhoLista++;
		} else {
			No novoNo = new No(dados);
			fim.setProximo(novoNo);
			fim = novoNo;
			tamanhoLista++;
		}

	}
	
	public No getCabeca(){
		return this.cabeca;
	}
	public No getFim(){
		return this.fim;
	}
	public No getNo(int indice) {
		No noAtual = cabeca;
		for (int x = 0; x < indice; x++) {
			noAtual = noAtual.getProximo();
		}
		return noAtual;
	}

	public void remover(int indice) {
		No noAtual;
		noAtual = getNo(indice);
		noAtual.getAnterior().setProximo(noAtual.getProximo());
		noAtual.getProximo().setAnterior(noAtual.getAnterior());

		tamanhoLista--;
	}

	public void listar() {

	}

	public void recuperar() {

	}

	public class IteradorListaDupla extends Iterador {

		private No indice;

		public IteradorListaDupla(No indice) {
			this.indice = indice;
		}

		@Override
		public Object proximo() {
			indice = indice.getProximo();
			return indice;
		}

		@Override
		public Object anterior() {
			indice = indice.getAnterior();
			return indice.getAnterior();
		}

		@Override
		public Object dado() {
			return indice.getDados();
		}

		@Override
		public boolean temProximo() {

			if (indice.getProximo() == null) {
				return false;
			} else {
				return true;
			}

		}

		@Override
		public boolean temAnterior() {
			if (indice.getAnterior() == null) {
				return false;
			} else {
				return true;
			}
		}

	}

}
