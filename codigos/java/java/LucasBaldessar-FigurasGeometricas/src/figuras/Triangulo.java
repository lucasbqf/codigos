package figuras;

public class Triangulo extends FiguraGeometrica {
	Ponto a;
	Ponto b;
	Ponto c;

	public static boolean eUmTriangulo(Ponto a, Ponto b, Ponto c) {
		if (a.distancia(b) + b.distancia(c) == a.distancia(c) || b.distancia(c) + c.distancia(a) == a.distancia(b)
				|| b.distancia(a) + a.distancia(c) == b.distancia(c))
			return false;
		return true;
	}

	public Triangulo(Ponto a, Ponto b, Ponto c) throws Exception {
		if (!eUmTriangulo(a, b, c)) {
			Exception e = new Exception("Pontos " + a + b + c + " n�o formam um triangulo.");
			throw e;
		}

		this.a = new Ponto(a);
		this.b = new Ponto(b);
		this.c = new Ponto(c);
	}

	public Triangulo(Triangulo t) {
		this.a = new Ponto(t.a);
		this.b = new Ponto(t.b);
		this.c = new Ponto(t.c);
	}

	@Override
	public float area() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float perimetro() {
		return a.distancia(b) + b.distancia(c) + c.distancia(a);
	}

	@Override
	public Ponto centro() {
		return new Ponto((a.getX() + b.getX() + c.getX()) / 3, (a.getY() + b.getY() + c.getY()) / 3);
	}

	public String toString() {
		return "[3 " + a + b + c + "]";
	}
}