package figuras;

public class Quadrado extends FiguraGeometrica {
	Ponto a;
	Ponto b;
	Ponto c;
	Ponto d;

	public static boolean eUmQuandrado(Ponto a, Ponto b, Ponto c, Ponto d) {
		if (a.distancia(b) != b.distancia(c) || c.distancia(d) != d.distancia(a) || b.distancia(c) != c.distancia(d))
			return false;
		return true;
	}

	public Quadrado(Ponto a, Ponto b, Ponto c, Ponto d) throws Exception {
		if (!eUmQuandrado(a, b, c, d)) {
			Exception e = new Exception("Pontos " + a + b + c + d + " n�o formam um quadrado.");
			throw e;
		}

		this.a = new Ponto(a);
		this.b = new Ponto(b);
		this.c = new Ponto(c);
		this.d = new Ponto(d);
	}

	public Quadrado(Quadrado q) {
		this.a = new Ponto(q.a);
		this.b = new Ponto(q.b);
		this.c = new Ponto(q.c);
		this.d = new Ponto(q.d);
	}

	@Override
	public float area() {
		return a.distancia(b) * c.distancia(d);
	}

	@Override
	public float perimetro() {
		return 4 * a.distancia(b);
	}

	@Override
	public Ponto centro() {
		return new Ponto((a.getX() + b.getX() + c.getX() + d.getX()) / 4,
				(a.getY() + b.getY() + c.getY() + d.getY()) / 4);
	}

	public String toString() {
		return "[4 " + a + b + c + d + "]";
	}
}