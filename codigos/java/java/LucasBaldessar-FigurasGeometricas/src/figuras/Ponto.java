package figuras;

public class Ponto {
	private float x;
	private float y;

	public Ponto(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Ponto(Ponto p) {
		this.x = p.x;
		this.y = p.y;
	}

	public float distancia(Ponto p) {
		return (float) Math.sqrt((this.x - p.x) * (this.x - p.x) + (this.y - p.y) * (this.y - p.y));
	}

	public String toString() {
		return String.format("(%.3f; %.3f)", x, y);
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
}