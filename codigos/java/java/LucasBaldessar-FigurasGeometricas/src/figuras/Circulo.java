package figuras;

public class Circulo extends FiguraGeometrica{
	private Ponto p;
	private float raio, pi;
	
	public Circulo(){
		p = new Ponto(0,0);
		this.raio = 2.0f;
		//this.pi = 3.14f;
		pi=3.14f;
	}
	
	public Circulo(Ponto p, float r){
		//construtor especifico
		this.p=p;
		raio=r;
		//pi=pp;
	}
	
	public Circulo(Circulo c){
		p = new Ponto(c.p);
		raio = c.raio;
		//pi = c.pi;
	}

	public Ponto getP() {
		return p;
	}

	public void setP(Ponto p) {
		this.p = p;
	}

	public float getRaio() {
		return raio;
	}

	public void setRaio(float raio) {
		this.raio = raio;
	}
	
	public float area(){
		return pi*raio*raio;
	}
	
	public float perimetro(){
		return 2*pi*raio;
	}
	
	public Ponto centro(){
		float x,y;
		x = (p.getX()+raio)/2;
		y = (p.getY()+raio)/2;
		return new Ponto(x,y);
	}
	
	public String toString(){
		return "Circulo com centro em " +centro()+ " e raio = " +raio;
	}
}
