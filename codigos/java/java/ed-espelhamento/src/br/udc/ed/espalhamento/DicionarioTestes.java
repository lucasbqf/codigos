package br.udc.ed.espalhamento;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class DicionarioTestes {

	@Test
	public void adicionaDevePassar() {
		final Dicionario dicionario = new Dicionario();
		dicionario.adiciona("UDC");
		dicionario.adiciona("anglo");
		dicionario.adiciona("alunos");

		Assert.assertTrue(dicionario.contem("UDC"));
		Assert.assertTrue(dicionario.contem("anglo"));
		Assert.assertTrue(dicionario.contem("alunos"));
	}

	@Test
	public void adicionaDevePassar2() {
		final Dicionario dicionario = new Dicionario();
		dicionario.adiciona("UDC");
		dicionario.adiciona("anglo");
		dicionario.adiciona("alunos");

		Assert.assertEquals(dicionario.tamanho(), 3);
	}

	@Test
	public void removeDevePassar() {
		final Dicionario dicionario = new Dicionario();
		dicionario.adiciona("UDC");
		dicionario.adiciona("anglo");
		dicionario.adiciona("alunos");

		dicionario.remove("alunos");
		Assert.assertEquals(dicionario.tamanho(),2);
	}

	@Test
	public void removeDevePassar2() {
		final Dicionario dicionario = new Dicionario();
		dicionario.adiciona("UDC");
		dicionario.adiciona("anglo");
		dicionario.adiciona("alunos");

		dicionario.remove("alunos");
		Assert.assertFalse(dicionario.contem("alunos"));
	}

	@Test
	public void todasDevePassar() {
		final Dicionario dicionario = new Dicionario();
		dicionario.adiciona("UDC");
		dicionario.adiciona("anglo");
		dicionario.adiciona("alunos");

		final List<String> palavras = dicionario.todas();
		Assert.assertEquals(palavras.size(), 3);
		Assert.assertTrue(dicionario.contem("UDC"));
		Assert.assertTrue(dicionario.contem("anglo"));
		Assert.assertTrue(dicionario.contem("alunos"));
	}
}
