package br.udc.ed.espalhamento;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Dicionario {
	int total = 0;

	public int calculaIndice(String palavra) {
		return palavra.toLowerCase().charAt(0) % QUANTIDADE_LETRAS;
	}

	private final int QUANTIDADE_LETRAS = 26;
	private ArrayList<LinkedList<String>> tabela = new ArrayList<LinkedList<String>>();

	public Dicionario() {
		for (int i = 0; i < QUANTIDADE_LETRAS; i++) {
			final LinkedList<String> lista = new LinkedList<String>();
			tabela.add(lista);
		}
	}

	public void adiciona(String palavra) {
		if (!this.contem(palavra)) {
			total++;
			int indice = this.calculaIndice(palavra);
			this.tabela.get(indice).add(palavra);
		}
	}

	public void remove(String palavra) {
		if (this.contem(palavra)) {
			total--;
			int indice = this.calculaIndice(palavra);
			this.tabela.get(indice).remove(palavra);
		} else {
			throw new RuntimeException("palavra " + palavra + "nao existe.");
		}
	}

	public boolean contem(String palavra) {
		int indice = this.calculaIndice(palavra);
		return this.tabela.get(indice).contains(palavra);
	}

	public List<String> todas() {
		final List<String> palavras = new ArrayList<String>();
		for (int i = 0; i < this.tabela.size(); i++) {
			palavras.addAll(this.tabela.get(i));
		}
		return palavras;
	}

	public int tamanho() {
		return total;
	}
	public void redimensiona (int novaCapacidade){
		final List<String> palavras =this.todas();
		this.tabela.clear();
		for(int i =0;i<novaCapacidade;i++){
			this.tabela.add(new LinkedList<String>());
		}
		for(String palavra : palavras){
			this.adiciona(palavra);
		}
	}
	public boolean VerRed(){
		int capacidade = this.tabela.size();
		double carga =(double) this.total/capacidade;
		
		
		if(carga> 0.75){
			this.redimensiona(capacidade*2);
		} else if (carga < 0.25){
			this.redimensiona(Math.max(capacidade/2,10));
		}
	}

}
