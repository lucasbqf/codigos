package br.edu.aula.ed.arvores.lista;

import java.util.HashSet;

import arvore.NodoAbstrato;

public class NodoLista<E> extends NodoAbstrato<E>{
	protected Set<NodoAbstrato<E>> filhos = new HashSet<>();
	protected int tamanhoArvore = 0;

	public NodoLista(E elemento, NodoAbstrato<E> nodoPai) {
		super(elemento, nodoPai);
	}
	public NodoLista( E elementoRaiz ) {
		super(elementoRaiz);
		this.tamanhoArvore = 1;
	}

	@Override
	public NodoLista<E> adicionar(E elemento) {
 		return null;
	}

}
