package br.edu.aula.ed.arvores.binaria;

import arvore.NodoAbstrato;

public abstract class NodoBinarioAbstrato<E> extends NodoAbstrato<E> {
	
	public NodoBinarioAbstrato(E elemento, NodoBinarioAbstrato<E> nodoPai){
		super(elemento,nodoPai);
	}
	public NodoBinarioAbstrato(E elementoRaiz){
		super(elementoRaiz);
	}
	public abstract NodoAbstrato<E> getEsquerdo();
	public abstract NodoAbstrato<E> getDireito();
	
}
