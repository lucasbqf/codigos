package arvore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class NodoAbstrato<E> {
	protected NodoAbstrato<E> raiz;
	protected NodoAbstrato<E> pai;
	protected E elemento;

	public abstract Collection<NodoAbstrato<E>> getFilhos();

	public NodoAbstrato(E elementoRaiz) {
		if (elementoRaiz == null)
			throw new IllegalArgumentException("uma arvore precisa de um elemnto raiz.");
		this.elemento = elementoRaiz;
		this.pai = null;
		this.raiz = this;
	}

	public NodoAbstrato(E elemento, NodoAbstrato<E> nodoPai) {
		if (nodoPai == null || elemento == null) {
			throw new IllegalArgumentException("um nodo valido precisa de um pai e um elemento.");
		}

	}

	public int grau() {
		return this.getFilhos() != null ? this.getFilhos().size() : 0;

	}

	public boolean externo() {
		return this.getFilhos() == null || this.getFilhos().isEmpty();
	}

	public boolean interno() {
		return !this.externo();
	}

	public NodoAbstrato<E> getPai() {
		if (this.pai == null) {
			throw new IndexOutOfBoundsException("N�o cont�m um pai. � a ra�z?");
		}
		return this.pai;
	}

	public boolean irmaoDe(NodoAbstrato<E> nodo) {
		return this.getPai().getFilhos().contains(nodo);
	}

	public E getElemento() {
		return this.elemento;
	}

	public void setElemento(E elemento) {
		this.elemento = elemento;
	}

	public NodoAbstrato<E> getRaiz() {
		if (this.raiz == null) {
			throw new IndexOutOfBoundsException("N�o existe nenhuma raiz definida.");
		}
		return (NodoAbstrato<E>) this.raiz;
	}

	public boolean raiz() {
		return this.getRaiz().equals(this);
	}

	@Override
	
	@SuppressWarnings("rawtypes")
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof NodoAbstrato))
			return false;

		final NodoAbstrato other = (NodoAbstrato) obj;
		if (this.elemento == null) {
			if (other.elemento != null)
				return false;
		} else if (!this.elemento.equals(other.elemento))
			return false;

		if (this.pai == null) {
			if (other.pai != null)
				return false;
		} else if (!this.pai.equals(other.pai))
			return false;

		return true;
	}

	public int hashCode() {
		final int primo = 31;
		int hash = 1;
		hash = primo * hash + ((elemento == null) ? 0 : elemento.hashCode());
		hash = primo * hash + ((pai == null) ? 0 : pai.hashCode());
		return hash;
	}
 	public boolean formaArestaCom(NodoAbstrato<E> nodoDestino) {
		return NodoAbstrato.formamAresta(this, nodoDestino);
	}

 	public static <E> boolean formamAresta(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino) {
		return nodoOrigem.getPai().equals(nodoDestino) || nodoDestino.getPai().equals(nodoOrigem);
	}
 	
 	public int profundidade() {
		return NodoAbstrato.profundidade(this);
	}

	public static <E> int profundidade(NodoAbstrato<E> nodo) {
		if (nodo.raiz()) {
			return 0;
		}
		return 1 + NodoAbstrato.profundidade(nodo.getPai());
	}
	public static <E> List<NodoAbstrato<E>> caminho(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino){
		final List<NodoAbstrato<E>> nodos = new ArrayList<>();
		if(nodoOrigem.getFilhos()){
			if(nodoFilho.equals(nodosDestino)){
				nodos.add(nodoDestino);
				return nodos;
			}else{
				nodos.addAll(nodoFilho.caminho(nodoDestino));
			}
		}
	}else if (nodoOrigem.descendenteDe(nodoDestino)){
		nodos.add(nodoOrigem);
		final NodoAbstrato<E> nodoPai = nodoOrigem.getPai();
		if( nodoPai.equals(nodoDestino)){
			nodos.add(nodosDestino);
			return nodos;
		} else{
			nodos.addAll(nodoPai.caminho(nodoDestino));
			
		}
	}
	public List<NodoAbstrato<E>> getFilho() {
		// TODO Auto-generated method stub
		return null;
	}

	return nodos;
}
