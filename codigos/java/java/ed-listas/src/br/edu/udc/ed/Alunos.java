package br.edu.udc.ed;

import java.util.Arrays;

import org.junit.Test;

public class Alunos {
	// Inicializando um array de Aluno com capacidade 100.
	private Aluno[] alunos = new Aluno[100];

	private int quantidade = 0;

	public void adiciona(Aluno aluno) {
		this.alunos[quantidade] = aluno;
		this.quantidade++;
	}

	public void adiciona(int posicao, Aluno aluno) {
	}

	public Aluno obtem(int posicao) {
		if (!this.posicaoOcupada(posicao)) {
			throw new IndexOutOfBoundsException("Posicao invalida");
		}
		return this.alunos[posicao];
	}

	private boolean posicaoOcupada(int posicao) {
		return posicao >= 0 && posicao < this.quantidade;

	}

	public void remove(int posicao) {
	}

	public boolean contem(Aluno aluno) {
		for (int i = 0; i < this.quantidade; i++) {
			if (aluno.equals(this.alunos[i])) {
				return true;
			}
		}
		return false;
	}

	public int tamanho() {
		return 0;
	}

	public String toString() {
		return Arrays.toString(alunos);
	}
}
