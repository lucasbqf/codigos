package br.edu.udc.ed;

public class Aluno {
	public String nome;
	public int creditos;

	public String toString() {
		return "O " + nome + "tem " + creditos + "creditos ";
	}
	@Override
	public boolean equals(Object object) {
		final Aluno outro = (Aluno) object;
		return nome.equals(outro.nome);
	}

}
