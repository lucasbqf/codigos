package br.edu.udc.ed;

import java.util.Arrays;

import org.junit.Test;

public class Lista {
	// Inicializando um array de Aluno com capacidade 100.
	private Object[] Object = new Object[100];

	private int quantidade = 0;

	public void adiciona(Aluno aluno) {
		this.Object[quantidade] = aluno;
		this.quantidade++;
	}

	public void adiciona(int posicao, Aluno aluno) {
	}

	public Object obtem(int posicao) {
		if (!this.posicaoOcupada(posicao)) {
			throw new IndexOutOfBoundsException("Posicao invalida");
		}
		return this.Object[posicao];
	}

	private boolean posicaoOcupada(int posicao) {
		return posicao >= 0 && posicao < this.quantidade;

	}

	public void remove(int posicao) {
	}

	public boolean contem(Aluno aluno) {
		for (int i = 0; i < this.quantidade; i++) {
			if (aluno.equals(this.Object[i])) {
				return true;
			}
		}
		return false;
	}

	public int tamanho() {
		return 0;
	}

	public String toString() {
		return Arrays.toString(Object);
	}
}
