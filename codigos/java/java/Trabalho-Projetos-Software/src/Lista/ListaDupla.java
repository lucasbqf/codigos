package Lista;

import Forma.Formageometrica;

public class ListaDupla {
	private class IteradorListaDupla extends Iterador {

		No noAtual;

		public IteradorListaDupla(No noAtual) {
			this.noAtual = noAtual;
		}

		public Object proximo() {

			if (noAtual != null) {
				Object dado = noAtual.getDado();
				noAtual = noAtual.getProximo();
				return dado;
			}
			return null;
		}

		public Object anterior() {
			if (noAtual != null) {
				Object dado = noAtual.getDado();
				noAtual = noAtual.getAnterior();
				return dado;
			}
			return null;
		}

		public Object dado() {
			if (noAtual != null)
				return noAtual.getDado();
			return null;
		}

	}

	private No inicio, fim;
	private int tamanho;

	public int getTamanho() {
		return tamanho;
	}

	public ListaDupla() {
		inicio = fim = null;
		tamanho = 0;
	}

	public Iterador getInicio() {
		IteradorListaDupla i = new IteradorListaDupla(this.inicio);
		return (Iterador) i;
	}

	public Iterador getFim() {
		IteradorListaDupla i = new IteradorListaDupla(this.fim);
		return (Iterador) i;
	}

	public void adicionarNaPosicao(int posicao, Formageometrica forma) throws Exception {// metodo
																							// com
																							// excessao

		if (posicao <= -1) {
			Exception excesao = new Exception("Posicao Invalida");// cria
																	// excessao
			throw excesao;
		}

		No no = new No(forma);

		if (forma == null) {
			return;
		}

		if (inicio == null) {
			inicio = fim = no;
			tamanho++;
			return;
		}

		if (posicao == 0) {
			no.setProximo(inicio);
			inicio.setAnterior(no);
			inicio = no;
			tamanho++;
			return;
		}

		if (posicao >= tamanho) {
			no.setAnterior(fim);
			fim.setProximo(no);
			fim = no;
			tamanho++;
			return;
		}

		No aux = inicio;
		int cont = 0;

		while (cont < posicao) {
			aux = aux.getProximo();
			cont++;
		}

		// verificar se a posicao esta antes ou depois do meio da lista
		// verificar se a lista so tem um elemento, senao vai dar pau
		no.setAnterior(aux.getAnterior());
		no.getAnterior().setProximo(no);
		no.setProximo(aux);
		aux.setAnterior(no);
		tamanho++;

	}

	public void RemoveNaPosicao(int posicao) throws Exception {

		if (posicao > this.tamanho || posicao <= -1) {
			Exception ex = new Exception("Posicao Invalida");
			throw ex;
		}
		
		if (posicao == this.tamanho-1){
			No atual1 = fim;
			atual1.setDado(atual1.getAnterior().getDado());
			atual1.setAnterior(atual1.getProximo().getProximo());
			this.tamanho--;
		}

		No atual = inicio;
		int quantidade = 0;
		while (atual != null) {
			if (posicao == quantidade) {
				atual.setDado(atual.getProximo().getDado());
				atual.setProximo(atual.getProximo().getProximo());
				this.tamanho--;
			}
			atual = atual.getProximo();
			quantidade++;
		}
	}

	public void adicionarInicio(Formageometrica forma) {
		try {
			adicionarNaPosicao(0, forma);
		} catch (Exception excesao) {
			excesao.addSuppressed(excesao);
		}

	}

	public boolean contem(Formageometrica forma) throws Exception {

		if (forma == null) {
			Exception ex = new Exception("Forma Geometrica Invalida");
			throw ex;
		}
		No atual = inicio;

		while (atual != null) {
			if (atual.getDado().equals(forma)) {
				return true;
			}
			atual = atual.getProximo();
		}
		return false;
	}

	public Formageometrica obtem(int posicao) throws Exception {

		if (posicao <= -1 || posicao > this.tamanho) {
			Exception ex = new Exception("Posicao invalida");
			throw ex;
		}

		No atual = inicio;
		int tamanho = 0;

		while (atual != null) {
			if (tamanho == posicao) {
				System.out.println(""+ atual.getDado());
				break;
			}
			atual = atual.getProximo();
			tamanho++;
		}
		return null;
	}

	private boolean posicaoOcupada(int posicao) {
		return posicao >= 0 && posicao < this.tamanho;
	}

}
