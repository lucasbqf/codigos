package Lista;

import Forma.Formageometrica;

public class No {
	private No anterior, proximo;
	private Formageometrica dado;
	
	public No(Formageometrica forma){
		anterior = proximo = null;
		dado = forma;
	}
	
	public No(No anterior, No proximo, Formageometrica forma){
		
	}
	
	public No getAnterior() {
		return anterior;
	}

	public void setAnterior(No anterior) {
		this.anterior = anterior;
	}

	public No getProximo() {
		return proximo;
	}

	public void setProximo(No proximo) {
		this.proximo = proximo;
	}

	public Formageometrica getDado() {
		return dado;
	}

	public void setDado(Formageometrica dado) {
		this.dado = dado;
	}
}

