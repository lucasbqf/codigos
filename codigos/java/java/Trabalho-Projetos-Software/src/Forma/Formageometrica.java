package Forma;


public abstract class Formageometrica {
	abstract public float area();
	abstract public float perimetro();
	abstract public Ponto centro();
	//toString()
	
	public abstract String toString();
	
	
}

//abstract � a base 