package Forma;


public class Quadrado extends Formageometrica{
	private Ponto p1,p2,p;
	private float lado;
	
	public Quadrado(){
		p = new Ponto(1,1);
		lado = 1.0f;
	}
	
	public Quadrado(Ponto p1, Ponto p2) throws Exception{
		if (p1 == null || p2 == null){
			
			Exception ex = new Exception ("Valor invalido");	
			throw ex;
		}
		
		this.p1 = p1;
		this.p2 = p2;
	}
	
	public Quadrado(Ponto p, float lado){
		//construtor especifico
		this.p=p;
		this.lado=lado;
	}
	
	public Quadrado(Quadrado q){
		this.p = new Ponto(q.p);
		this.lado=q.lado;
	}

	public Ponto getP() {
		return p;
	}

	public void setP(Ponto p) {
		this.p = p;
	}

	public float getLado() {
		return lado;
	}

	public void setLado(float lado) {
		this.lado = lado;
	}

	public float area(){
		return lado*lado;
	}
	
	public float perimetro(){
		return lado*4;
	}
	
	public Ponto centro(){
		float x,y;
		x = (p.getX()+lado)/2;//(p.getX()/2)+lado);
		y = (p.getY()+lado)/2;
		return new Ponto(x,y);
	}
	
	@Override
	public String toString() {
	return "Quadrado;" + p1 + ";"+ p2 + "";
	}
	
	
}
