package Forma;


public class Ponto {
	private float x, y;
	
	public Ponto(){
		x=y=0;
	}
	
	public Ponto(float x, float y){
		this.x=x;
		this.y=y;
	}
	
	public Ponto(Ponto p){
		p.x=x;
		p.y=y;
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float distancia(Ponto p){
		return (float) Math.sqrt((p.x-x)*(p.x-x)+(p.y-y)*(p.y-y));
	}
	
	public String toString(){
		return String.format("%f;%f", x, y);
	}
	
}
// ponto � composi��o

/*
quadrado "�" forma geometrica
retangulo "�" forma geometrica
triangulo "�" forma geometrica
circulo "�" forma geometrica
 */
