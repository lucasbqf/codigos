package br.edu.ed.mapas;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MapaTabelaHerois {
	private List<List<AssociacaoHeroi>> tabela = new ArrayList<>();
	private int quantidade = 0;

	public MapaTabelaHerois() {
		for (int i = 0; i < 100; i++) {
			this.tabela.add(new LinkedList<AssociacaoHeroi>());
		}
	}

	public int calculaIndiceDaTabela(String qrcode) {
		return Math.abs(qrcode.hashCode()) % this.tabela.size();
	}

	public void adiciona(String qrcode, Heroi heroi) {
		this.verificaCarga();
		if (!this.contem(qrcode)) {
			final int indice = this.calculaIndiceDaTabela(qrcode);
			final List<AssociacaoHeroi> lista = this.tabela.get(indice);
			lista.add(new AssociacaoHeroi(qrcode, heroi));
			this.quantidade++;
			return;
		} else {
			throw new IllegalArgumentException("Ja existe heroi com esse qrcode!");
		}
	}

	public void remove(String qrcode) {
		final int indice = this.calculaIndiceDaTabela(qrcode);

		final List<AssociacaoHeroi> lista = this.tabela.get(indice);
		for (int i = 0; i < lista.size(); i++) {
			final AssociacaoHeroi associacao = lista.get(i);
			if (associacao.getQRCode().equals(qrcode)) {
				lista.remove(i);
				this.quantidade--;
				return;
			}
		}
		throw new IllegalArgumentException("N�o existe her�i com este QRCode");
	}

	public Heroi obtem(String qrcode) {
		final int indice = this.calculaIndiceDaTabela(qrcode);
		final List<AssociacaoHeroi> lista = this.tabela.get(indice);

		for (int i = 0; i < lista.size(); i++) {
			final AssociacaoHeroi associacao = lista.get(i);
			if (associacao.getQRCode().equals(qrcode)) {
				return associacao.getHeroi();
			}
		}

		throw new IllegalArgumentException("N�o existe her�i com este qrcode.");
	}

	public boolean contem(String qrcode) {
		final int indice = this.calculaIndiceDaTabela(qrcode);
		final List<AssociacaoHeroi> lista = this.tabela.get(indice);
		for (int i = 0; i < lista.size(); i++) {
			final AssociacaoHeroi associacao = lista.get(i);
			if (associacao.getQRCode().equals(qrcode)) {
				return true;
			}
		}
		return false;
	}

	public List<AssociacaoHeroi> pegaTodas() {
		List<AssociacaoHeroi> lista = new ArrayList<AssociacaoHeroi>();

		for (int i = 0; i < this.tabela.size(); i++) {
			lista.addAll(this.tabela.get(i));
		}

		return lista;
	}

	public List<AssociacaoHeroi> todas()// Lista todos os itens da listas
	{
		final List<AssociacaoHeroi> elementos = new ArrayList<AssociacaoHeroi>();
		for (int i = 0; i < this.tabela.size(); i++) {
			elementos.addAll(this.tabela.get(i));
		}
		return elementos;
	}

	public void redimencionar(int novaCpacidade) {

		final List<AssociacaoHeroi> palavras = this.todas();
		this.tabela.clear();

		this.tabela.clear();
		for (int i = 0; i < novaCpacidade; i++) {
			this.tabela.add(new LinkedList<AssociacaoHeroi>());
		}
		for (AssociacaoHeroi palavra : palavras) {
			final int indice = this.calculaIndiceDaTabela(palavra.getQRCode());
			this.tabela.get(indice).add(palavra);

		}

	}

	public void verificaCarga() {
		int capacidade = this.tabela.size();
		double carga = (double) this.quantidade / capacidade;
		if (carga > 0.75) {
			this.redimencionar(capacidade * 2);
		} else if (carga < 0.75) {
			this.redimencionar(Math.max(capacidade / 2, 10));
		}

	}

	public int tamanho() {
		return this.quantidade;
	}

}
