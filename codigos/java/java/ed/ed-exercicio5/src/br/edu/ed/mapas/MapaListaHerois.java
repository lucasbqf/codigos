package br.edu.ed.mapas;

import java.util.ArrayList;
import java.util.List;

public class MapaListaHerois {

	private List<AssociacaoHeroi> associacoes = new ArrayList<AssociacaoHeroi>();

	public void adiciona(String qrcode, Heroi heroi) {
		if (!this.contem(qrcode)) {
			final AssociacaoHeroi associacao = new AssociacaoHeroi(qrcode, heroi);
			this.associacoes.add(associacao);
		}
	}

	public void remove(String qrcode) {
		if (!this.contem(qrcode)) {
			throw new IllegalArgumentException("Chave nao existente!");
		} else {
			for (int i = 0; i < this.associacoes.size(); i++) {
				final AssociacaoHeroi associacao = this.associacoes.get(i);
				if (qrcode.equals(associacao.getQRCode())) {
					this.associacoes.remove(i);
					break;
				}
			}
		}
	}

	public Heroi obtem(String qrcode) {

		for (AssociacaoHeroi associacao : this.associacoes) {
			if (qrcode.equals(associacao.getQRCode())) {
				return associacao.getHeroi();
			}
		}
		throw new IllegalArgumentException("Chave n�o existente");

	}

	public boolean contem(String qrcode) {
		for (AssociacaoHeroi associacao : this.associacoes) {
			if (qrcode.equals(associacao.getQRCode())) {
				return true;
			}
		}
		return false;
	}

	public int tamanho() {
		return this.associacoes.size();
	}
}
