package br.edu.ed.mapas.testes;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.edu.ed.mapas.AssociacaoHeroi;
import br.edu.ed.mapas.Heroi;
import br.edu.ed.mapas.MapaTabelaHerois;

public class MapaListaHeroiTeste {
	@Test
	public void adicionaTesteUmDevePassar() {
		final MapaTabelaHerois mapa = new MapaTabelaHerois();
		final Heroi akuma = new Heroi();
		akuma.setNome("akuma");
		akuma.setEspecialidade("Satsui No Hadou");
		akuma.setForca(10f);
		akuma.setFraqueza("Lust");
		akuma.setVoa(false);
		akuma.setHumano(true);
		mapa.adiciona("hadou", akuma);
		Assert.assertEquals(mapa.tamanho(), 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void adicionaTesteDoisDevePassar() {
		final MapaTabelaHerois mapa = new MapaTabelaHerois();
		final Heroi akuma = new Heroi();
		akuma.setNome("akuma");
		akuma.setEspecialidade("Satsui No Hadou");
		akuma.setForca(10f);
		akuma.setFraqueza("Lust");
		akuma.setVoa(false);
		akuma.setHumano(true);
		mapa.adiciona("hadou", akuma);
		mapa.adiciona("hadou", akuma);
		Assert.assertEquals(mapa.tamanho(), 1);
	}

	@Test
	public void removeTesteUmDevePassar() {
		final MapaTabelaHerois mapa = new MapaTabelaHerois();
		final Heroi akuma = new Heroi();
		akuma.setNome("akuma");
		akuma.setEspecialidade("Satsui No Hadou");
		akuma.setForca(10f);
		akuma.setFraqueza("Lust");
		akuma.setVoa(false);
		akuma.setHumano(true);
		mapa.adiciona("hadou", akuma);
		mapa.remove("hadou");
		System.out.println(mapa.tamanho());
		Assert.assertEquals(mapa.tamanho(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void removeTesteDoisDevePassar() {
		final MapaTabelaHerois mapa = new MapaTabelaHerois();
		mapa.remove("aaaaaa");
		System.out.println(mapa.tamanho());
		Assert.assertEquals(mapa.tamanho(), 0);
	}

	@Test
	public void pegaTodasTesteUmDevePassar() {
		final MapaTabelaHerois mapa = new MapaTabelaHerois();
		for (int i = 0; i < 50; i++) {
			mapa.adiciona("Teste" + i, new Heroi());
		}
		List<AssociacaoHeroi> Lista = mapa.pegaTodas();
		for (AssociacaoHeroi associacao : Lista) {
			System.out.println(associacao.getQRCode());
		}
	}

	@Test
	public void calculaIndiceDaTabelaTesteUmDevePassar() {
		final MapaTabelaHerois tabela = new MapaTabelaHerois();
		int a = tabela.calculaIndiceDaTabela("akuma");
		int b = tabela.calculaIndiceDaTabela("ryu");
		assertTrue(a != b);
	}

	@Test
	public void calculaIndiceDaTabelaTesteDoisDevePassar() {
		final MapaTabelaHerois tabela = new MapaTabelaHerois();
		int a = tabela.calculaIndiceDaTabela("akuma");
		int b = tabela.calculaIndiceDaTabela("akuma");
		assertTrue(a == b);
	}

	@Test
	public void contemTesteUmDevePassar() {
		final MapaTabelaHerois tabela = new MapaTabelaHerois();
		tabela.adiciona("HomemAranha", new Heroi());
		assertTrue(tabela.contem("akuma"));
	}

	@Test
	public void contemTesteDoisDevePassar() {
		final MapaTabelaHerois tabela = new MapaTabelaHerois();
		assertTrue(!tabela.contem("akuma"));
	}

	@Test
	public void redimencionarTesteUmDevePassar() {
		final MapaTabelaHerois tabela = new MapaTabelaHerois();
		for (int i = 0; i < 1000; i++) {
			tabela.adiciona("Herois" + i, new Heroi());
		}
		Assert.assertEquals(1000, tabela.tamanho());
	}

	@Test
	public void redimencionarTesteDoisDevePassar() {
		final MapaTabelaHerois tabela = new MapaTabelaHerois();
		for (int i = 0; i < 10; i++) {
			tabela.adiciona("Herois" + i, new Heroi());
		}
		Assert.assertEquals(10, tabela.tamanho());
	}

	@Test
	public void ObtemTesteUmDevePassar() {
		final MapaTabelaHerois tabela = new MapaTabelaHerois();
		final Heroi ryu = new Heroi();
		ryu.setNome("ryu");
		ryu.setEspecialidade("hadoken");
		ryu.setForca(6f);
		ryu.setFraqueza("compaixao");
		ryu.setHumano(true);
		ryu.setVoa(false);
		tabela.adiciona("ryu", ryu);
		tabela.obtem("ryu");
		assertTrue(ryu == tabela.obtem("ryu"));
	}
}
