package br.edu.aula.ed.arvores.testes;

import static org.junit.Assert.*;

import org.junit.Test;

import br.edu.aula.ed.arvores.NodoAbstrato;
import br.edu.aula.ed.arvores.NodoLista;
import junit.framework.Assert;

public class NodoListaTeste {

	@Test
	public void grauTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("bla");
		final NodoLista<String> a = raiz.adicionar("foo");
		final NodoLista<String> b = raiz.adicionar("hu3");
		Assert.assertEquals(2, raiz.grau());
		Assert.assertEquals(0, b.grau());
	}

	@Test
	public void grauTesteDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("bla");
		final NodoLista<String> a = raiz.adicionar("foo");
		final NodoLista<String> b = raiz.adicionar("hu3");
		final NodoLista<String> c = raiz.adicionar("tar");
		final NodoLista<String> d = c.adicionar("bal");
		Assert.assertEquals(3, raiz.grau());
		Assert.assertEquals(0, d.grau());
		Assert.assertEquals(1, c.grau());
	}

	@Test
	public void irmaoDeTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("bla");
		final NodoLista<String> a = raiz.adicionar("foo");
		final NodoLista<String> b = raiz.adicionar("hu3");
		final NodoLista<String> c = raiz.adicionar("tar");
		final NodoLista<String> d = c.adicionar("bal");
		assertFalse(a.irmaoDe(d));
	}

	@Test
	public void irmaoDeTesteDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("bla");
		final NodoLista<String> a = raiz.adicionar("foo");
		final NodoLista<String> b = raiz.adicionar("hu3");
		final NodoLista<String> c = raiz.adicionar("tar");
		final NodoLista<String> d = c.adicionar("bal");
		assertTrue(b.irmaoDe(d));
	}

	@Test
	public void externoTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("bla");
		final NodoLista<String> a = raiz.adicionar("foo");
		final NodoLista<String> b = raiz.adicionar("hu3");
		final NodoLista<String> c = raiz.adicionar("tar");
		final NodoLista<String> d = c.adicionar("bal");;
		assertTrue(d.externo());
	}

	@Test
	public void externoTesteDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("bla");
		final NodoLista<String> a = raiz.adicionar("foo");
		final NodoLista<String> b = raiz.adicionar("hu3");
		final NodoLista<String> c = raiz.adicionar("tar");
		final NodoLista<String> d = c.adicionar("bal");
		assertFalse(c.externo());
	}

	@Test
	public void internoTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("bla");
		final NodoLista<String> a = raiz.adicionar("foo");
		final NodoLista<String> b = raiz.adicionar("hu3");
		final NodoLista<String> c = raiz.adicionar("tar");
		final NodoLista<String> d = c.adicionar("bal");;
		assertFalse(a.interno());
	}

	@Test
	public void internoTesteDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertTrue(iron.interno());
	}

	@Test
	public void raizTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertFalse(tres.raiz());
	}

	@Test
	public void raizTesteDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertTrue(raiz.raiz());
	}

	@Test
	public void formaArestaComTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertFalse(tres.formaArestaCom(spider));
	}

	@Test
	public void formaArestaComDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertTrue(iron.formaArestaCom(spider));
	}

	@Test
	public void getPaiTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertEquals(iron, spider.getPai());
		assertEquals(raiz, iron.getPai());
	}

	@Test
	public void getPaiTesteDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertNotEquals(raiz, spider.getPai());
		assertNotEquals(tres, spider.getPai());
	}

	@Test
	public void getRaizTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertEquals(raiz, spider.getRaiz());
		assertEquals(raiz, serieDois.getRaiz());
	}

	@Test
	public void getRaizTesteDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertNotEquals(iron, spider.getRaiz());
		assertNotEquals(tres, spider.getRaiz());
	}

	@Test
	public void getElementoTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");

		assertEquals(iron.getPai().getElemento(), raiz.getElemento());
	}

	@Test
	public void getElementoTesteDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");
		assertNotEquals(iron, iron.getElemento());
	}

	@Test
	public void setElementoTesteUmDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");

		iron.setElemento("Batman");
		assertEquals("Batman", iron.getElemento());
	}

	@Test
	public void setElementoTesteDoisDevePassar() {
		final NodoLista<String> raiz = new NodoLista<String>("Doctor");
		final NodoLista<String> serieDois = raiz.adicionar("CapitãoAmerica");
		final NodoLista<String> tres = raiz.adicionar("ViuvaNegra");
		final NodoLista<String> iron = raiz.adicionar("HomemDeFerro");
		final NodoLista<String> spider = iron.adicionar("spiderMan");

		iron.setElemento("Batman");

		assertNotEquals("HomemDeFerro", iron.getElemento());
	}

}
