package br.edu.aula.ed.arvores;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class NodoAbstrato<E> {
	
	protected NodoAbstrato<E> raiz;
	protected NodoAbstrato<E> pai;
	protected E elemento;
	
	public NodoAbstrato(E elementoRaiz){
		if(elementoRaiz == null){
			throw new IllegalArgumentException("Uma arvore precisa de um elemento raiz.");
		}
		this.setElemento(elementoRaiz);
		this.pai = null;
		this.raiz = this; //que confuso?? a rais eh ela mesma
	}
	
	public NodoAbstrato(E elemento, NodoAbstrato<E> nodoPai){
		if(nodoPai==null || elemento==null){
			throw new IllegalArgumentException("Um nodo valido precisa de um pai e um elemento."); //ternary if
		}
		this.pai = nodoPai;
		this.raiz = nodoPai.getRaiz();
		this.setElemento(elemento);
	}
	
	public int grau(){
		return this.getFilhos() != null ? this.getFilhos().size() : 0;
	}
	
	public boolean externo(){
		return this.getFilhos() == null || this.getFilhos().isEmpty();
	}
	
	public boolean interno(){
		return !this.externo();
	}
	
	public NodoAbstrato<E> getPai(){
		if(this.pai == null){
			throw new IndexOutOfBoundsException("Nao contem um pai. E a raiz?");
		}
		return this.pai;
	}
	
	public boolean irmaoDe(NodoAbstrato<E> nodo){
		return this.getPai().getFilhos().contains(nodo);
	}
	
	public E getElemento(){
		return this.elemento;
	}
	
	public void setElemento(E elemento){
		this.elemento = elemento;
	}
	
	public NodoAbstrato<E> getRaiz(){
		if(this.raiz == null){
			throw new IndexOutOfBoundsException("Nao existe nenhuma raiz definida.");
		}
		return (NodoAbstrato<E>) this.raiz;
	}
	
	public boolean raiz(){
		return this.getRaiz().equals(this);
	}
	
	@SuppressWarnings("rawtypes")
	public boolean equals(Object obj){
		if(this == obj) return true;
		if(obj == null) return false;
		if(!(obj instanceof NodoAbstrato)) return false;//se o objeto nao for uma instacia de NodoAbstrato, retorn falso.
		
		final NodoAbstrato other = (NodoAbstrato) obj;
		if(this.elemento == null){
			if(other.elemento != null) return false;
		} else if(!this.elemento.equals(other.elemento)) return false;
		
		if(this.pai == null){
			if(other.pai != null) return false;
		} else if(!this.pai.equals(other.pai)) return false;
		
		return true;
	}
	
	public int hashCode(){
		final int primo = 31;
		int hash = 1;
		hash = primo * hash + ((elemento == null)? 0 : elemento.hashCode());
		hash = primo * hash + ((pai == null)? 0 : pai.hashCode());
		return hash;
	}
	
	public boolean formaArestaCom(NodoAbstrato<E> nodoDestino){
		return NodoAbstrato.formamAresta(this, nodoDestino);
	}
	
	public static <E> boolean formamAresta(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino){
		return nodoOrigem.getPai().equals(nodoDestino) || nodoDestino.getPai().equals(nodoOrigem);
	}
	
	public int profundidade(){
		return NodoAbstrato.profundidade(this);
	}
	
	public static <E> int profundidade(NodoAbstrato <E> nodo){
		if(nodo.raiz()){
			return 0;
		}
		return 1 + NodoAbstrato.profundidade(nodo.getPai());
	}
	/*
	 * Implementar a altura
	 */
	public int altura(){
		return 0;
	}
		
	/*O codigo do caminho vai cair na prova.
	 * 
	 * 
	 * 
	 */
	public List<NodoAbstrato<E>> caminho(NodoAbstrato<E> nodoDestino){
		return NodoAbstrato.caminho(this, nodoDestino);
	}
	
	public static <E> List<NodoAbstrato<E>> caminho(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino){
		final List<NodoAbstrato<E>> nodos = new ArrayList<>();
		
		if(nodoOrigem.ancestralDe(nodoDestino)){
			nodos.add(nodoOrigem);
			for(NodoAbstrato<E> nodoFilho : nodoOrigem.getFilhos()){
				if(nodoFilho.equals(nodoDestino)){
					nodos.add(nodoDestino);
					return nodos; //achou o destino
				} else{
					nodos.addAll(nodoFilho.caminho(nodoDestino));
				}
			}
		} else if(nodoOrigem.descendenteDe(nodoDestino)){
			nodos.add(nodoOrigem);
			final NodoAbstrato<E> nodoPai = nodoOrigem.getPai();
			
			if(nodoPai.equals(nodoDestino)){
				nodos.add(nodoDestino);
				return nodos; //achou o destino
			} else{
				nodos.addAll(nodoPai.caminho(nodoDestino));
			}
		}
		return nodos;
	}
	
	public boolean ancestralDe(NodoAbstrato<E> nodo){
		return NodoAbstrato.temAscendencia(this, nodo);
	}
	
	public static <E> boolean temAscendencia(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino){
		if(nodoOrigem.raiz()){
			return true;
		}
		if(nodoOrigem.interno()){
			for(NodoAbstrato<E> nodoFilho : nodoOrigem.getFilhos()){
				if(nodoFilho.equals(nodoDestino) || NodoAbstrato.temAscendencia(nodoFilho, nodoDestino)){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean descendenteDe(NodoAbstrato<E> nodo){
		return NodoAbstrato.temDescendencia(this, nodo);
	}
	
	public static <E> boolean temDescendencia(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino){
		if(nodoOrigem.raiz()){
			return false;
		}
		
		final NodoAbstrato<E> nodoPai = nodoOrigem.getPai();
		if(nodoPai.equals(nodoDestino)){
			return true;
		}
		return NodoAbstrato.temDescendencia(nodoPai, nodoDestino);
	}
	
	public int comprimento(NodoAbstrato<E> nodoDestino){
		return NodoAbstrato.caminho(this, nodoDestino).size();
	}
	
	public abstract int tamanhoArvore();
	public abstract NodoAbstrato<E> adicionar(E elemento);
	public abstract Collection<NodoAbstrato<E>> getFilhos();
}
