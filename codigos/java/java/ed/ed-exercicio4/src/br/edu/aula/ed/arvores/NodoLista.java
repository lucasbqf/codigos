package br.edu.aula.ed.arvores;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class NodoLista<E> extends NodoAbstrato<E>{
	protected Set<NodoAbstrato<E>> filhos = new HashSet<>();
	protected int tamanhoArvore = 0;

	public NodoLista(E elemento, NodoLista<E> nodoPai) {
		super(elemento, nodoPai);
	}
	public NodoLista( E elementoRaiz ) {
		super(elementoRaiz);
		this.tamanhoArvore = 1;
	}
	@Override
	public NodoLista<E> adicionar(E elemento) {

		final NodoAbstrato<E> nodoFilho = new NodoLista<E>(elemento, this);
		this.filhos.add(nodoFilho);

		final NodoLista<E> raiz = (NodoLista<E>) super.getRaiz();
		raiz.tamanhoArvore++;
		
		return (NodoLista<E>) nodoFilho;
	}
	
	@Override
	public int tamanhoArvore() {
		return this.tamanhoArvore;
	}
	public int grau(){
		return this.getFilhos() != null ? this.getFilhos().size() : 0;
	}
	
	public boolean externo(){
		return this.getFilhos() == null || this.getFilhos().isEmpty();
	}
	
	public boolean interno(){
		return !this.externo();
	}
	
	public NodoAbstrato<E> getPai(){
		if(this.pai == null){
			throw new IndexOutOfBoundsException("Nao contem um pai. E a raiz?");
		}
		return this.pai;
	}
	
	public boolean irmaoDe(NodoAbstrato<E> nodo){
		return this.getPai().getFilhos().contains(nodo);
	}
	
	public E getElemento(){
		return this.elemento;
	}
	
	public void setElemento(E elemento){
		this.elemento = elemento;
	}
	
	public NodoAbstrato<E> getRaiz(){
		if(this.raiz == null){
			throw new IndexOutOfBoundsException("Nao existe nenhuma raiz definida.");
		}
		return (NodoAbstrato<E>) this.raiz;
	}
	
	public boolean raiz(){
		return this.getRaiz().equals(this);
	}

	@Override
	public Set<NodoAbstrato<E>> getFilhos() {
		return this.filhos;
	}
	@SuppressWarnings("rawtypes")
	public boolean equals(Object obj){
		if(this == obj) return true;
		if(obj == null) return false;
		if(!(obj instanceof NodoAbstrato)) return false;//se o objeto nao for uma instacia de NodoAbstrato, retorn falso.
		
		final NodoAbstrato other = (NodoAbstrato) obj;
		if(this.elemento == null){
			if(other.elemento != null) return false;
		} else if(!this.elemento.equals(other.elemento)) return false;
		
		if(this.pai == null){
			if(other.pai != null) return false;
		} else if(!this.pai.equals(other.pai)) return false;
		
		return true;
	}
	
	public int hashCode(){
		final int primo = 31;
		int hash = 1;
		hash = primo * hash + ((elemento == null)? 0 : elemento.hashCode());
		hash = primo * hash + ((pai == null)? 0 : pai.hashCode());
		return hash;
	}
	
	public boolean formaArestaCom(NodoAbstrato<E> nodoDestino){
		return NodoAbstrato.formamAresta(this, nodoDestino);
	}
	
	public static <E> boolean formamAresta(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino){
		return nodoOrigem.getPai().equals(nodoDestino) || nodoDestino.getPai().equals(nodoOrigem);
	}
	



}
