package br.edu.ed.mapas.testes;

import static org.junit.Assert.*;
import org.junit.Assert;
import org.junit.Test;

import br.edu.ed.mapas.Associacao;
import br.edu.ed.mapas.Documento;

import br.edu.ed.mapas.MapaGenerico;

import br.edu.ed.mapas.Pessoa;

public class MapaGenericoCadastrodePessoasTeste {

	@Test
	public void AdicionaTesteUmDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();
		final Pessoa pessoa = new Pessoa();
		final Documento doc = new Documento();
		pessoa.setNome("akuma");
		pessoa.setSexo("masculino");
		doc.setCpf("0898998");
		doc.setRg("123467");
		mapa.adiciona(doc, pessoa);
		System.out.println(mapa.imprimePessoas());

		Assert.assertEquals(mapa.tamanho(), 1);

	}

	@Test
	public void adicionaTesteDoisDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();
		final Pessoa pessoa = new Pessoa();
		final Documento doc = new Documento();
		pessoa.setNome("akuma");
		pessoa.setSexo("masculino");
		doc.setCpf("0898998");
		doc.setRg("123467");
		mapa.adiciona(doc, pessoa);
		mapa.adiciona(doc, pessoa);
		System.out.println(mapa.imprimeDocumento());
		Assert.assertEquals(mapa.tamanho(), 1);
	}

	@Test
	public void removeTesteUmDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();
		final Pessoa pessoa = new Pessoa();
		final Documento doc = new Documento();

		final Pessoa hue = new Pessoa();
		final Documento documento = new Documento();

		hue.setNome("ryu");
		hue.setSexo("masculino");
		documento.setCpf("123456789");
		documento.setRg("6789045678");

		
		pessoa.setNome("akuma");
		pessoa.setSexo("masculino");
		doc.setCpf("0898998");
		doc.setRg("123467");
		mapa.adiciona(documento, hue);
		mapa.adiciona(doc, pessoa);
		mapa.remove(doc);

		System.out.println(mapa);
		Assert.assertEquals(mapa.tamanho(), 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void removeTesteDoisDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();
		final Pessoa pessoa = new Pessoa();
		final Documento doc = new Documento();

		final Pessoa hue = new Pessoa();
		final Documento documento = new Documento();

		hue.setNome("ryu");
		hue.setSexo("masculino");
		documento.setCpf("123456789");
		documento.setRg("6789045678");
		mapa.adiciona(documento, hue);

		
		mapa.remove(doc);

		
		System.out.println(mapa);
		Assert.assertEquals(mapa.tamanho(), 1);
	}

	@Test
	public void redimencionaTabelaTesteUmDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();

		final long tempoInicialParaDez = System.currentTimeMillis();

		for (int i = 0; i < 10; i++) {
			mapa.adiciona(new Documento("teste" + i, "teste" + i + 1), new Pessoa());
		}
		final long tempoFinalParaDez = System.currentTimeMillis();

		final long tempoTotalParaDez = tempoFinalParaDez - tempoInicialParaDez;

		final long tempoInicialParaMil = System.currentTimeMillis();

		for (int i = 0; i < 1000; i++) {
			mapa.adiciona(new Documento("teste" + i, "teste" + i + 1), new Pessoa());
		}
		final long tempoFinalParaMil = System.currentTimeMillis();

		final long tempoTotalParaMil = tempoFinalParaMil - tempoInicialParaMil;
		System.out.println(tempoTotalParaDez);
		System.out.println(tempoTotalParaMil);
		Assert.assertTrue(tempoTotalParaDez < tempoTotalParaMil);
	}

	@Test
	public void redimencionaTabelaTesteDoisDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();
		final long tempoInicial = System.currentTimeMillis();

		for (int i = 0; i < 10000; i++) {
			mapa.adiciona(new Documento("teste" + i, "teste" + i + 1), new Pessoa());
		}
		final long tempoFinal = System.currentTimeMillis();
		final long tempoTotal = tempoFinal - tempoInicial;
		System.out.println(tempoTotal);
		Assert.assertEquals(mapa.tamanho(), 10000);
	}

	@Test
	public void obtemTesteUmDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();
		final Pessoa pessoa = new Pessoa();
		final Documento doc = new Documento();

		final Pessoa hue = new Pessoa();
		final Documento documento = new Documento();

		hue.setNome("ryu");
		hue.setSexo("masculino");
		documento.setCpf("123456789");
		documento.setRg("6789045678");

		
		pessoa.setNome("akuma");
		pessoa.setSexo("masculino");
		doc.setCpf("0898998");
		doc.setRg("123467");
		mapa.adiciona(documento, hue);
		mapa.adiciona(doc, pessoa);


		Assert.assertEquals(pessoa, mapa.obtem(doc));

	}

	@Test(expected = IllegalArgumentException.class)
	public void obtemTesteDoisDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();
		final Pessoa pessoa = new Pessoa();
		final Documento doc = new Documento();
		final Pessoa hue = new Pessoa();
		final Documento documento = new Documento();

		hue.setNome("ryu");
		hue.setSexo("masculino");
		documento.setCpf("123456789");
		documento.setRg("6789045678");

		
		pessoa.setNome("akuma");
		pessoa.setSexo("masculino");
		doc.setCpf("0898998");
		doc.setRg("123467");
		mapa.adiciona(documento, hue);
		mapa.adiciona(doc, pessoa);


		mapa.adiciona(doc, pessoa);
		Assert.assertEquals(hue, mapa.obtem(documento));
	}

	@Test
	public void contemTesteDoisDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();
		final Pessoa pessoa = new Pessoa();
		final Documento doc = new Documento();

		final Pessoa hue = new Pessoa();
		final Documento documento = new Documento();

		hue.setNome("ryu");
		hue.setSexo("masculino");
		documento.setCpf("123456789");
		documento.setRg("6789045678");

		
		pessoa.setNome("akuma");
		pessoa.setSexo("masculino");
		doc.setCpf("0898998");
		doc.setRg("123467");
		mapa.adiciona(documento, hue);
		mapa.adiciona(doc, pessoa);


		Assert.assertTrue(mapa.contem(documento));

	}

	@Test
	public void contemTesteUmDevePassar() {
		final MapaGenerico mapa = new MapaGenerico();
		final Pessoa pessoa = new Pessoa();
		final Documento doc = new Documento();

		final Pessoa hue = new Pessoa();
		final Documento documento = new Documento();

		hue.setNome("ryu");
		hue.setSexo("masculino");
		documento.setCpf("123456789");
		documento.setRg("6789045678");

		
		pessoa.setNome("akuma");
		pessoa.setSexo("masculino");
		doc.setCpf("0898998");
		doc.setRg("123467");
		mapa.adiciona(documento, hue);
		mapa.adiciona(doc, pessoa);


		Assert.assertFalse(mapa.contem(doc));

	}

}
