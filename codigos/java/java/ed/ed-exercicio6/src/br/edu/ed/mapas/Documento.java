package br.edu.ed.mapas;

public class Documento {
	private String cpf;
	private String rg;

	public Documento(String cpf, String rg) {
		this.cpf = cpf;
		this.rg = rg;
	}
	public Documento() {
		this.cpf = null;
		this.rg = null;
	}
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	@Override
	public String toString() {
		return "CPF:" + this.cpf + "RG:" + this.rg;
	}

}
