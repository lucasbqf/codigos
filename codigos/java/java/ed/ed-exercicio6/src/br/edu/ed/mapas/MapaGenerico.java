package br.edu.ed.mapas;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MapaGenerico<C, V> {
	private List<List<Associacao<C, V>>> tabela = new ArrayList<>();
	private int quantidade = 0;

	public MapaGenerico() {
		for (int i = 0; i < 100; i++) {
			this.tabela.add(new LinkedList<Associacao<C, V>>());
		}
	}

	public void adiciona(C chave, V valor) {
		if (this.contem(chave)) {
			this.remove(chave);
		}
		final int indice = this.calculaIndiceDaTabela(chave);
		final List<Associacao<C, V>> lista = this.tabela.get(indice);
		lista.add(new Associacao<>(chave, valor));
		this.quantidade++;
	}

	public void remove(C chave) {
		final int indice = this.calculaIndiceDaTabela(chave);

		final List<Associacao<C, V>> lista = this.tabela.get(indice);
		for (int i = 0; i < lista.size(); i++) {
			final Associacao<C, V> associacao = lista.get(i);
			if (associacao.getChave().equals(chave)) {
				lista.remove(i);
				this.quantidade--;
				return;
			}
		}
		throw new IllegalArgumentException("N�o existe valor com esta Chave");
	}

	public V obtem(C chave) {
		final int indice = this.calculaIndiceDaTabela(chave);
		final List<Associacao<C, V>> lista = this.tabela.get(indice);

		for (int i = 0; i < lista.size(); i++) {
			final Associacao<C, V> associacao = lista.get(i);
			if (associacao.getChave().equals(chave)) {
				return associacao.getValor();
			}
		}

		throw new IllegalArgumentException("N�o existe valor com esta chave.");
	}

	public boolean contem(C chave) {
		final int indice = this.calculaIndiceDaTabela(chave);

		final List<Associacao<C, V>> lista = this.tabela.get(indice);
		for (int i = 0; i < lista.size(); i++) {
			final Associacao<C, V> associacao = lista.get(i);
			if (associacao.getChave().equals(chave)) {
				return true;
			}
		}
		return false;
	}

	public int tamanho() {
		return this.quantidade;
	}

	private void redimensionaTabela(int novaCapacidade) {
		final List<Associacao<C, V>> associacoes = new ArrayList<>();
		for (List<Associacao<C, V>> associacoesTabela : this.tabela) {
			associacoes.addAll(associacoesTabela);
		}
		this.tabela.clear();

		for (int i = 0; i < novaCapacidade; i++) {
			this.tabela.add(new LinkedList<Associacao<C, V>>());
		}
		for (Associacao<C, V> associacao : associacoes) {
			final int indice = this.calculaIndiceDaTabela(associacao.getChave());
			this.tabela.get(indice).add(associacao);
		}
	}

	private int calculaIndiceDaTabela(C chave) {
		return Math.abs(chave.hashCode()) % this.tabela.size();
	}

	private void verificaCarga() {
		int capacidade = this.tabela.size();
		double carga = (double) this.quantidade / capacidade;

		if (carga > 0.75) {
			this.redimensionaTabela(capacidade * 2);
		} else if (carga < 0.25) {
			this.redimensionaTabela(Math.max(capacidade / 2, 100));
		}
	}

	public List<Associacao<C, V>> pegaTodos() {
		List<Associacao<C, V>> objetos = new ArrayList<Associacao<C, V>>();
		for (int i = 0; i < this.tabela.size(); i++) {
			objetos.addAll(this.tabela.get(i));
		}
		return objetos;
	}	
	public List<V> pegaPessoa(){
		List<V> pessoas =  new ArrayList<V>();
		List<Associacao<C, V>> objetos = new ArrayList<Associacao<C, V>>();
		objetos = this.pegaTodos();
		for (int i = 0; i < objetos.size(); i++) {
			pessoas.add(objetos.get(i).getValor());
		}
		return pessoas;		
	}
	public String imprimePessoas(){
		return this.pegaPessoa().toString();
	}
	public List<C> pegaDocumento(){
		List<C> documentos =  new ArrayList<C>();
		List<Associacao<C, V>> objetos = new ArrayList<Associacao<C, V>>();
		objetos = this.pegaTodos();
		for (int i = 0; i < objetos.size(); i++) {
			documentos.add(objetos.get(i).getChave());
		}
		return documentos;		
	}
	public String imprimeDocumento(){
		return this.pegaDocumento().toString();
	}
	@Override
	public String toString() {
		return this.pegaTodos().toString();
	}

}
