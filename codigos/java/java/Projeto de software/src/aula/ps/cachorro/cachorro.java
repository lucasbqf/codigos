package aula.ps.cachorro;

public class cachorro {

	private float peso;
	private String cor;
	private boolean castrado;

	public cachorro(float peso, String cor) {
		if (peso > 0.1f && peso < 200f)
			this.peso = peso;
		this.castrado = false;
	}

	public void latir() {
		System.out.println("auau");
	}

	public void comer(float gramasRacao) {
		peso += gramasRacao;
	}

	public float getpeso() {
		return peso;
	}

	public String toString() {
		return String.format("objeto cachorro tem peso %.3f e cor %s", peso, cor);
	}

}
