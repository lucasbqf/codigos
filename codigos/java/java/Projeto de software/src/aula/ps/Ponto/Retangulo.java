package aula.ps.Ponto;

public class Retangulo {
	private Ponto p;
	private  float base,altura;
	
	public Retangulo (){
		this.p= new Ponto(0,0);
		base= 2.0f;
		altura = 1.0f;
	}
	
	public Retangulo(Ponto p, float b,float a){
		this.p=p;
		base=b;
		altura=a;
	}
	public Retangulo(Retangulo r){
		p=new Ponto(r,p);
		base=r.base;
		altura=r.altura;
	}
	
	
}
