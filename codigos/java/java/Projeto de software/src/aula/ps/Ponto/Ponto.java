package aula.ps.Ponto;

public class Ponto {
	private int x;
	private int y;

	public Ponto() {
		x = y = 0;
	}

	public Ponto(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public float distancia(Ponto p) {
		return (float) Math.sqrt((p.x - x) * (p.x - x) + (p.y - y) * (p.y - y));
	}

	public String toString() {
		return String.format("(%d;%d)", x, y);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	boolean alinhadoHor(Ponto p){
		if(this.y==p.y)
			return true;
		return false;
	}
	boolean alinhadoVert(Ponto p){
		if(this.x==p.x)
			return true;
		return false;
	}
	int distHor(Ponto p){
		return this.x-p.x;
	}
}
