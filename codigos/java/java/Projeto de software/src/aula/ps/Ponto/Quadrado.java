package aula.ps.Ponto;

public class Quadrado {
	private Ponto p1;
	private Ponto p2;
	public Quadrado(){
		p1=new Ponto(0,0);
		p2=new Ponto(1,1);
	}
	public Quadrado(Ponto p1,Ponto p2){
		if(p1==p2){
			this.p1=p1;
			this.p2=new Ponto (p1.getY()+1,p1.getY()+1);
		}
		if(p1.getX()==p2.getX()&&p1.getY()==p2.getY()){
			this.p2= new Ponto(p1.getX()+1,p1.getY()+1);
		}
	}
	public Ponto getP1() {
		return p1;
	}
	public void setP1(Ponto p1) {
		this.p1 = p1;
	}
	public Ponto getP2() {
		return p2;
	}
	public void setP2(Ponto p2) {
		this.p2 = p2;
	}
	
	
	public float lado(){
		if(p1.alinhadoHor(p2) || p1.alinhadoVert(p2)){
			return (float)p1.distancia(p2);
		}
		return 0;
	}
	
}

