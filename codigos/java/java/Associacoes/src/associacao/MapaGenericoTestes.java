package associacao;

import org.junit.Test;

@Test
public class MapaGenericoTestes {
	final MapaGenerico<String, Heroi > herois = new MapaGenerico<>();
	final Heroi spiderman = new Heroi("homem aranha",10F,true,false);
	herois.adiciona("XXXMMOO",spiderman);
	final heroi goku = new Heroi("Goku", 80000F,false,true);
	herois.adiciona("GODGOKU",goku);
	
	Assert.assertEquals(2, herois.tamanho());
	Assert.assertTrue();
	

}
