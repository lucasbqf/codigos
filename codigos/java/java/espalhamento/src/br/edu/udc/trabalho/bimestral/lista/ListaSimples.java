package br.edu.udc.trabalho.bimestral.lista;

public class ListaSimples {
	Pessoa ini, fim;
	int contador = 0;

	public ListaSimples() {
		this.ini = null;
		this.fim = null;
	}

	public boolean verificaLista() {
		return contador == 0;
	}

	public void adiciona(String nome, int idade, int cpf, String sexo) {
		Pessoa novaPessoa = new Pessoa();
		novaPessoa.setNome(nome);
		novaPessoa.setIdade(idade);
		novaPessoa.setCpf(cpf);
		novaPessoa.setSexo(sexo);

		if (verificaLista()) {
			ini = fim = novaPessoa;

		} else {
			fim.setProximo(novaPessoa);
			fim = fim.getProximo();
			novaPessoa.setProximo(null);
		}
		contador++;
	}

	public void imprimirLista() {
		Pessoa navegador = ini;
		while (navegador != null) {
			System.out.println("NOME : " + navegador.getNome());
			System.out.println("IDADE: " + navegador.getIdade());
			System.out.println("CPF  : " + navegador.getCpf());
			System.out.println("SEXO : " + navegador.getSexo());
			System.out.println("");
			navegador = navegador.getProximo();
		}
	}
}
