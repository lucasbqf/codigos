package br.edu.udc.trabalho.bimestral.lista;

public class Pessoa {
	private String nome;
	private int idade;
	private int cpf;
	private String sexo;
	private Pessoa proximo;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		if (idade < 0) {
			System.out.println("Idade Invalida");
		} else {
			this.idade = idade;
		}
	}

	public int getCpf() {
		return cpf;
	}

	public void setCpf(int cpf) {
		this.cpf = cpf;

	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Pessoa getProximo() {
		return proximo;
	}

	public void setProximo(Pessoa proximo) {
		this.proximo = proximo;
	}
}
