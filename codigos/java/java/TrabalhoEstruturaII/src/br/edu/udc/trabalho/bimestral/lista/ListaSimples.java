package br.edu.udc.trabalho.bimestral.lista;

public class ListaSimples {
	Pessoa ini,fim; // Apontadores do tipo pessoa
	int contador = 0;
	
	public ListaSimples(){		// Quando eu criar a minha lista, inicio e fim aponta para null
		this.ini = null;
		this.fim = null;
	}
	
	public boolean verificaLista(){		// verifica se a lista esta vazia
		return contador == 0;
	}
	
	
	public void adiciona(String nome, int idade,int cpf, String sexo){
		Pessoa novaPessoa = new Pessoa();
		novaPessoa.setNome(nome);
		novaPessoa.setIdade(idade);
		novaPessoa.setCpf(cpf);
		novaPessoa.setSexo(sexo);
		
		if(verificaLista()){
			ini = fim = novaPessoa; // se minha lista estiver vazia, inicio e fim apontam para o primeiro objeto criado
		}
		else{
			fim.setProximo(novaPessoa); // o proximo recebe o novo objeto
			fim = fim.getProximo();		// o fim antigo recebe o fim do novo objeto
			novaPessoa.setProximo(null);
		}
		contador++;
	}
	
	public void imprimirLista(){
		Pessoa navegador = ini;
		while (navegador != null){
			System.out.println("NOME : "+navegador.getNome());
			System.out.println("IDADE: "+navegador.getIdade());
			System.out.println("CPF  : "+navegador.getCpf());
			System.out.println("SEXO : "+navegador.getSexo());
			System.out.println("");
			navegador = navegador.getProximo();
		}
	}
}
