package br.edu.udc.trabalho.bimestral.lista;

public class Principal {
	public static void main(String[] args){	
		ListaSimplesEncadeada lista = new ListaSimplesEncadeada();		
		
		lista.adiciona("Sergio", 21, 123456, "M");
		lista.adiciona("Joaozinho", 21, 567891, "F");
		lista.adiciona("HAHA", 15, 2134567, "F");
		lista.adiciona("AmbrosioVTNC", 20, 20, "F");
		lista.varrerLista();
	}
}
