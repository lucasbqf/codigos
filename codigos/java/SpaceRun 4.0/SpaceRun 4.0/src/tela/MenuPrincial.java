package tela;

import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JPanel;

public class MenuPrincial extends JPanel {

	JButton novoJogo, estatisticas, sair;
	public MenuPrincial() {
		novoJogo = new JButton("NOVO JOGO");
		estatisticas = new JButton("ESTATISTICAS");
		sair= new JButton("SAIR"); 
	}
	public void paintComponent(Graphics g)
	{
		
		add(novoJogo);
		add(estatisticas);
		add(sair);
	}
}
