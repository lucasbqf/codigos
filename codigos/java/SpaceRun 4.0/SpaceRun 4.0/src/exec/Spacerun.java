package exec;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Label;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;

import engine.Engine;
import engine.Parametros;
//import engine.Parametros;
import scripts.OuvinteMouse;
import tela.GameScreen;
import tela.MenuPrincial;
import tela.Painel;

@SuppressWarnings("unused")
public class Spacerun {

	public static void main(String[] args) {
		//objetos do jogo
		GameScreen telaJogo;
		Painel desenhosjogo;
		JProgressBar lifeBar;
		JLabel score;
		
		telaJogo = new GameScreen();
		telaJogo.setResizable(false);
		
		OuvinteMouse ouvinte = new OuvinteMouse();
		
		desenhosjogo = new Painel();
		desenhosjogo.setSize(Parametros.x, Parametros.y);
		
		lifeBar = new JProgressBar(0,Parametros.vidaPlayer);
		lifeBar.setStringPainted(true);
		lifeBar.setForeground(Color.red);
		
		score = new JLabel();
		
		
		telaJogo.add(desenhosjogo);
		telaJogo.add(lifeBar);
		lifeBar.setString("VIDA");
		telaJogo.add(score,BorderLayout.SOUTH);
		desenhosjogo.addMouseListener(ouvinte);
		desenhosjogo.addMouseMotionListener(ouvinte);

		// inicializacao do motor do jogo
		
		
		
		//la�o eterno do jogo
		while (true){
			Engine motor = new Engine();
			while (motor.getPlayerLife() >= 1) {
				// ouvinte.atirou=false;

				// um pouco de gambiarra mas elimina a necessidade de mais um
				// objeto no array de desenhos
				desenhosjogo.jogadorX = ouvinte.mouseX;
				desenhosjogo.jogadorY = ouvinte.mouseY;
				motor.Update(ouvinte.mouseX, ouvinte.mouseY, ouvinte.atirou);

				// atualiza as posicoes aonde estao cada objeto
				desenhosjogo.Refreshobj(motor.pontosobj);
				
				lifeBar.setValue(motor.vidaPlayer);
				score.setText("pontuacao: "+motor.pontuacao);
				// redesenha toda a tela com todas as posicoes atualizadas
				telaJogo.repaint();

				// funcao para parar o jogo por 15 milisegundos
				try {
					Thread.sleep(15);
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
			}
		}
		
	
	}
	
	

}
