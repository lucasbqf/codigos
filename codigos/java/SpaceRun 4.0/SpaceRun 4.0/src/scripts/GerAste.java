package scripts;

import objects.Obj;

import java.util.Random;

import objects.Asteroide;
/**
 * classe para a geracao dos asteroides, gerando em posicoes aleatorias em relacao ao eixo x
 * @author lucas
 *
 */
public class GerAste extends Obj {
	public Asteroide gerarAst(int vector2[]){
		Random rand = new Random();
			Asteroide ast = new Asteroide();
			ast.setVector2(vector2[0],vector2[1]);
			ast.setVelocidade(1+ rand.nextInt(5));
			ast.setRaioColisao(10+(rand.nextInt(8)*5));
			return ast; 
	}


}
