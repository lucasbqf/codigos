package exec;

import engine.Engine;
import scripts.OuvinteMouse;
import tela.GameScreen;
import tela.Painel;

public class Spacerun {

	public static void main(String[] args) {
		//inicializacao dos objetos para o desenho do jogo
		GameScreen telaJogo = new GameScreen();
		Painel desenhosjogo = new Painel();
		OuvinteMouse ouvinte = new OuvinteMouse();
		telaJogo.add(desenhosjogo);
		desenhosjogo.addMouseListener(ouvinte);
		desenhosjogo.addMouseMotionListener(ouvinte);
		
		//inicializacao do motor do jogo
		Engine motor = new Engine();
		
		//la�o eterno para o jogo rodar
		while (true) {
			
			
			desenhosjogo.jogadorX=ouvinte.mouseX;
			desenhosjogo.jogadorY=ouvinte.mouseY;
			motor.Update(ouvinte.mouseX,ouvinte.mouseY,ouvinte.atirou);
			
			// atualiza as posicoes aonde estao cada objeto
			desenhosjogo.Refreshobj(motor.pontosobj);
			//redesenha toda a tela com todas as posicoes atualizadas
			telaJogo.repaint();
			
			
			try {
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
	}

}
