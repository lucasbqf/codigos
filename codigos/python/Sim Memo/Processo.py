#programa criado por Lucas Baldessar de Quiroz Fontes
class TpProcesso:
    # construtor
    def __init__(self, nome, PID, tamanho, cor):
        self.nome = nome
        self.PID = PID
        self.tamanho=tamanho
        self.cor = cor


    def GetKey(self):
        return self.PID

    #"overide" equivalente do "toString" do java , para o python
    def __repr__(self):
        return '{}: NOME:{} | PID:{} | TAM:{} | COR:{}'.format(self.__class__.__name__,
                                                          self.nome,
                                                          self.PID,
                                                          self.tamanho,
                                                          self.cor)