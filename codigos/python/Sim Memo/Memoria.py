from Particao import *
from Vazio import *
import random
#programa criado por Lucas Baldessar de Quiroz Fontes


def rdmCor():
    cor = ("#%06x" % random.randint(0, 0xFFFFFF))
    return cor


# listaMemoria=[]
class Memoria:
    global listaMemoria
    listaMemoria = []

    def __init__(self, tamanhoMemoria, tipo):
        global listaMemoria
        self.tamanhoMemoria = tamanhoMemoria
        self.listaMemoria = listaMemoria
        listaMemoria.append(TpParticao(Vazio, 0, tamanhoMemoria))

    def uniParticao(self):
        global listaMemoria
        i = 0
        for TpProcesso in listaMemoria:
            #print("andando pela posicao " + str(i))
            #print("")
            if len(self.listaMemoria) > 1 and i + 1 < len(self.listaMemoria):
                if self.listaMemoria[i].processo == Vazio and self.listaMemoria[i + 1].processo == Vazio:
                    self.listaMemoria[i].tamanho = self.listaMemoria[i].tamanho + self.listaMemoria[i + 1].tamanho
                    self.listaMemoria[i].final = self.listaMemoria[i + 1].final
                    del self.listaMemoria[i + 1]
            i = i + 1

    def adiciona(self, processo):
        global listaMemoria
        self.uniParticao()
        i = 0
        for TpProcesso in listaMemoria:
            # se encontra uma particao vazia , com tamanho suficiente, e o tamanho nao estourar a memoria
            if listaMemoria[i].processo == Vazio and listaMemoria[i].tamanho >= processo.tamanho:
                # adiciona o processo na particao
                listaMemoria[i].processo = processo
                # cor aleatoria
                listaMemoria[i].processo.cor = rdmCor()
                # redimenciona a particao
                tmParticaoLivre = listaMemoria[i].tamanho - processo.tamanho
                # iguala o tamanho da particao com o processo
                listaMemoria[i].tamanho = processo.tamanho
                # a particao deve comecar aonde a anterior terminou
                listaMemoria[i].posicao = listaMemoria[i - 1].final
                # define aonde a particao acaba
                listaMemoria[i].final = listaMemoria[i].posicao + listaMemoria[i].tamanho
                # cria uma particao nova e vazia
                listaMemoria.insert(i + 1, TpParticao(Vazio, listaMemoria[i].final, tmParticaoLivre))
                # define o aonde a particao vazia acaba
                listaMemoria[i + 1].final = listaMemoria[i + 1].posicao + tmParticaoLivre
                break
            #print(listaMemoria[i])
            i = i + 1
        self.uniParticao()

    def desfragmentar(self):
        i = 0
        tamanhosobra = 0
        while i <= (len(self.listaMemoria)-2):
            #print((len(listaMemoria)))
            #print(i)
            if self.listaMemoria[i].processo == Vazio:
                tamanhosobra += int(listaMemoria[i].tamanho)
                del self.listaMemoria[i]
            if self.listaMemoria[i].processo is not Vazio:
                i += 1

        i = 0
        for posicao in range(0,(len(listaMemoria)-1)):
            if self.listaMemoria[i].final is not self.listaMemoria[i + 1].posicao:
                self.listaMemoria[i + 1].posicao = self.listaMemoria[i].final
                self.listaMemoria[i + 1].final = self.listaMemoria[i + 1].posicao + self.listaMemoria[i + 1].tamanho
            i += 1
        self.listaMemoria[int(len(self.listaMemoria)-1)].tamanho = self.listaMemoria[int(len(self.listaMemoria)-1)].tamanho+tamanhosobra
        self.listaMemoria[int(len(self.listaMemoria)-1)].final=self.listaMemoria[int(len(self.listaMemoria)-1)].posicao+self.listaMemoria[int(len(self.listaMemoria)-1)].tamanho


    def remover(self, PID):
        global listaMemoria
        self.uniParticao()
        i = 0
        # itera pela lista
        if PID > 1:
            for Particao in listaMemoria:
                #print(listaMemoria[i])
                # procura pela particao com o PID em questao
                if int(listaMemoria[i].processo.PID) == int(PID):
                    # quando encontra, retira o processo
                    listaMemoria[i].processo = Vazio
                    break
                i = i + 1
            self.uniParticao()
            #print("finalizou removcer")

    def desligar(self):
        global listaMemoria
        i = 1
        # itera pela lista
        for Particao in range(0,(len(listaMemoria)-1)):
            listaMemoria[i].processo = Vazio
            i = i + 1
        self.uniParticao()
        listaMemoria[1].processo=Vazio

# TESTE DA FUNCAO MEMORIA
def teste():
    x = Memoria(64, 1)
    i = 0
    x.adiciona((TpProcesso("hue", 1, 20, "black")))
    x.adiciona((TpProcesso("bla", 2, 15, "red")))
    x.adiciona((TpProcesso("foo", 3, 10, "blue")))
    x.adiciona((TpProcesso("egg", 4, 5, "green")))
    x.remover(2)
    x.adiciona(TpProcesso("eita", 9, 5, "red"))
    x.adiciona(TpProcesso("Finaleira", 666, 14, "red"))
    for object in x.listaMemoria:
        print(x.listaMemoria[i])
        i = i + 1
    # x.desfragmentar()
    print("apos desfragmentacao: \n \n")
    i = 0
    for object in x.listaMemoria:
        print(x.listaMemoria[i])
        i = i + 1

# teste()
