#programa criado por Lucas Baldessar de Quiroz Fontes

import tkinter
from telaProcessos import *

desenhosMemoria = []
contPID=2
def TELA2(lista,tmMemoria):

    tela2 = tkinter.Tk()
    tela2.configure(bg="white")
    tela2.title("SIM MEMO")
    canvasMemoria = Canvas(tela2, width=302, height=52)

    def TamRelativo(valor):
        x=(valor*300)/tmMemoria
        return round(x)

    def desenhamemoria():
        i = 0
        desenhosMemoria.clear()
        for processo in lista.listaMemoria:
            desenhosMemoria.append(canvasMemoria.create_rectangle(int(TamRelativo(lista.listaMemoria[i].posicao)), 1, int(TamRelativo(lista.listaMemoria[i].final)), 52, fill=lista.listaMemoria[i].processo.cor))
            i = i + 1
    def NovoProcesso():
        global contPID
        lista.adiciona(TpProcesso(nomeProcesso.get(),int(contPID),int(tamanhoProcesso.get()),"black"))
        contPID=contPID+1
        desenhamemoria()
        refresh()


    def RemoverProcesso():
        PID=PIDProcesso.get()
        if int(PID) >1:
            lista.remover(int(PID))
            desenhamemoria()
            print(PID)
            refresh()

    def desliga():
        lista.desligar()
        desenhamemoria()

    def desfragmenta():
        lista.desfragmentar()
        desenhamemoria()

    SimMemo = tkinter.Label(tela2,bg="white", text="Sim MEMO")
    NomePrograma = tkinter.Label(tela2,bg="white", text="Simulador de memoria")
    canvasMemoria.create_rectangle(2, 2, 300, 50, fill="white")
    espaco = tkinter.Label(tela2,bg="white")
    inserenome=Label(tela2,text="Nome:",bg="white")
    nomeProcesso=Entry()
    insereTamanho=Label(tela2,text="Tamanho",bg="white")
    tamanhoProcesso=Entry()
    adicionarProcesso = Button(tela2, text="adicionar processo",command=NovoProcesso,bg="white")
    PIDProcesso=Entry()
    removeProcesso= Button(tela2,text="remove processo",command=RemoverProcesso,bg="white")
    Desfragmentar= Button(tela2,text="Desfragmentar!",command=desfragmenta,bg="white")
    Desligar=Button(tela2,text="Desligar",command=desliga,bg="white")

    Label(tela2,text="PROCESSOS:",bg="white").grid(row=2,column=3)
    Label(tela2,text="nome:",bg="white").grid(row=3,column=2)
    Label(tela2,text="PID:",bg="white").grid(row=3,column=3)
    Label(tela2,text="tamanho:",bg="white").grid(row=3,column=4)
    Label(tela2,text="cor:",bg="white").grid(row=3,column=5)

    def refresh():
        global i
        i=0
        linha=0
        for object in lista.listaMemoria:
            Label(tela2,text="        ",bg="white").grid(row=i+4,column=2)
            Label(tela2,text="        ",bg="white").grid(row=i+4,column=3)
            Label(tela2,text="        ",bg="white").grid(row=i+4,column=4)
            Label(tela2,text="        ",bg="white").grid(row=i+4,column=5)
            if lista.listaMemoria[i].processo.PID is not 0:
                Label(tela2,bg="white",text="%s"%(lista.listaMemoria[i].processo.nome)).grid(row=linha+4,column=2)
                Label(tela2,bg="white",text="%s"%(lista.listaMemoria[i].processo.PID)).grid(row=linha+4,column=3)
                Label(tela2,bg="white",text="%s"%(lista.listaMemoria[i].processo.tamanho)).grid(row=linha+4,column=4)
                Label(tela2,text="COR",fg=(lista.listaMemoria[i].processo.cor),bg=(lista.listaMemoria[i].processo.cor)).grid(row=linha+4,column=5)
                linha=linha+1
            i=i+1



    SimMemo.grid(row=1,column=1)
    NomePrograma.grid(row=2,column=1)
    canvasMemoria.grid(row=3,column=1)
    espaco.grid(row=4,column=1)
    inserenome.grid(row=5,column=1)
    nomeProcesso.grid(row=6,column=1)
    insereTamanho.grid(row=7,column=1)
    tamanhoProcesso.grid(row=8,column=1)
    adicionarProcesso.grid(row=9,column=1)
    PIDProcesso.grid(row=10,column=1)
    removeProcesso.grid(row=11,column=1)
    Desfragmentar.grid(row=12,column=1)
    #Desligar.grid(row=13,column=1)

    desenhamemoria()
    refresh()
    tela2.mainloop()


#TESTE DE TELA
#TELA2(lista, listaMemoria, 100000)