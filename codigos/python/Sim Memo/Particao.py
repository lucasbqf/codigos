#programa criado por Lucas Baldessar de Quiroz Fontes
from Processo import *


class TpParticao:
    def __init__(self, processo, posicao,tamanho):
        self.processo = processo
        self.tamanho = tamanho
        self.posicao = posicao
        self.final = processo.tamanho + posicao

    def getkey(self):
        return self.processo.PID
    def getTamanho(self):
        return self.processo.tamanho
    def __repr__(self):
        return '{}: PROCESSO:|( {} )| TAMANHO:{} | POSICAO:{} | FINAL:{}'.format(self.__class__.__name__,
                                                          self.processo,
                                                          self.tamanho,
                                                          self.posicao,
                                                          self.final)
