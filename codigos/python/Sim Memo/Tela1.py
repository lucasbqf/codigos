#programa criado por Lucas Baldessar de Quiroz Fontes
import tkinter



tamanhoLista=0
tipo=1
varTamanhoSO=0

def TELA1():
    global tamanhoLista
    def verificaTamanho():
        global tamanhoLista
        global varTamanhoSO
        if ((int(tamanhoMemoria.get()) < 640 or int(tamanhoMemoria.get()) > 16384)):
            erroMemoria.configure(text="valores precisam estar entre 640(k) e 16384(k) (16M) e sem Letras!")
        else:
            if int(tamanhoSO.get()) > 0 and int(tamanhoSO.get()) < int(tamanhoMemoria.get()):
                varTamanhoSO = int(tamanhoSO.get())
                tamanhoLista = int(tamanhoMemoria.get())
                tela.destroy()
            else:
                erroTmSO.configure(text="tamanho do SO precisa ser > 0 e < "+tamanhoMemoria.get() )


    def first():
        global tipo
        tipo = 1
        #print (tipo)

    def best():
        global tipo
        tipo = 2
        #print (tipo)


    def worst():
        global tipo
        tipo = 3
        #print (tipo)
    def getTamanhoSO():
        global varTamanhoSO
        varTamanhoSO=int(tamanhoSO.get())

    tela = tkinter.Tk()
    tela.title("SIM MEMO")
    SimMemo = tkinter.Label(tela, text="Sim MEMO")
    NomePrograma = tkinter.Label(tela, text="Simulador de memoria")
    LabelMemoria = tkinter.Label(tela, text="insira o tamanho da memoria:")
    espaco = tkinter.Label(tela)
    tamanhoMemoria = tkinter.Entry(tela)
    btTamanhoMemoria = tkinter.Button(tela, text="ok", command=verificaTamanho)
    btFirst = tkinter.Button(tela, text=" First-Fit  ", command=first)
    btBest = tkinter.Button(tela, text=" Best-Fit  ", command=best)
    btWorst = tkinter.Button(tela, text="Worst-Fit", command=worst)
    tipoMemoria = tkinter.Label(tela, text="Selecione A estrategia:")
    erroMemoria = tkinter.Label(tela, text="")
    infoSO = tkinter.Label(text="tamanho do SO:")
    tamanhoSO = tkinter.Entry(tela)
    erroTmSO=tkinter.Label(text="")

    SimMemo.grid(row=1,column=2)
    NomePrograma.grid(row=2,column=2)
    LabelMemoria.grid(row=3,column=1)
    tamanhoMemoria.grid(row=3,column=2)
    erroMemoria.grid(row=4,column=2)
    tipoMemoria.grid(row=5,column=1)
    btFirst.grid(row=5,column=2)
    btBest.grid(row=6,column=2)
    btWorst.grid(row=7,column=2)
    espaco.grid(row=8,column=2)
    infoSO.grid(row=9,column=1)
    tamanhoSO.grid(row=9,column=2)
    erroTmSO.grid(row=10,column=2)
    btTamanhoMemoria.grid(row=11,column=2)
    espaco.grid(row=12,column=2)
    espaco.grid(row=12,column=3)

    tela.mainloop()

def getTamanho():
    return tamanhoLista
def getTipo():
    return tipo
def getTmSo():
    return varTamanhoSO



#TESTE DE TELA
#TELA1(1,64)